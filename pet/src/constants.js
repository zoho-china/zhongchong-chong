import React from "react";

export const API_URL = 'https://www.zohoapis.com.cn';
export const contractColumn = [{
    title: '行号',
    dataIndex: 'Name',
}, {
    title: '外销合同号',
    dataIndex: 'Foreignsacontract.Name',
}, {
    title: '客户订单号',
    dataIndex: 'AccountOrderNo'
}, {
    title: '商品编码',
    dataIndex: 'Product.Product_Name',
}, {
    title: 'Productid',
    dataIndex: 'Product.id',
    className: 'hidden-column'
}, {
    title: '客户货号',
    dataIndex: 'AccountprtNo',
}, {
    title: '唛头',
    dataIndex: 'ShippingMark'
}, {
    title: '内部代码',
    dataIndex: 'internalcode'
}, {
    title: '品牌',
    dataIndex: 'Brand'
}, {
    title: '英文货名',
    dataIndex: 'Englishname',
    ellipsis: true,

}, {
    title: '中文品名',
    dataIndex: 'Productname'
}, {
    title: '英文规格',
    dataIndex: 'Specification'
}, {
    title: '中文规格',
    dataIndex: 'Chinesespecification'
}, {
    title: '单价',
    dataIndex: 'Unit_Price'
}, {
    title: '中文包装描述',
    dataIndex: 'ChinesePackagedesp',
    ellipsis: true,

}, {
    title: '数量',
    dataIndex: 'Quantity'
}, {
    title: '中文生产要求(材质)',
    dataIndex: 'Chptrequirements_material',
    ellipsis: true,

}, {
    title: '合计金额',
    dataIndex: 'TotalAmount'
}, {
    title: '中文成份',
    dataIndex: 'Chineseingredients'
}, {
    title: '每箱数量',
    dataIndex: 'QuantityperCarton'
}, {
    title: '类别',
    dataIndex: 'Category'
}, {
    title: '包装单位',
    dataIndex: 'Packaginunit'
}, {
    title: 'Packaginunit',
    dataIndex: 'Packaginunit.id',
    className: 'hidden-column'
}, {
    title: '单位',
    dataIndex: 'Unit'
}, {
    title: 'unit_id',
    dataIndex: 'Unit.id',
    className: 'hidden-column'
}, {
    title: '长CM',
    dataIndex: 'Lengthofouterbox'
}, {
    title: '宽CM',
    dataIndex: 'Cartonwidth'
}, {
    title: '高CM',
    dataIndex: 'Heightofouterbox'
}, {
    title: '体积CBM',
    dataIndex: 'Volume'
}, {
    title: '总体积',
    dataIndex: 'TotalVolume'
}, {
    title: '入数',
    dataIndex: 'Nuofboxesinpg'
}, {
    title: '内盒数',
    dataIndex: 'Nuofboxeinnerbox'
}, {
    title: '箱数',
    dataIndex: 'NumberofCartons'
}, {
    title: '净重',
    dataIndex: 'Netweight'
}, {
    title: '总净重',
    dataIndex: 'TotalNetWeight'
}, {
    title: '毛重',
    dataIndex: 'Grossweight'
}, {
    title: '总毛重',
    dataIndex: 'TotalGrossWeight'
}, {
    title: '备注',
    dataIndex: 'Remarks'
}, {
    title: '未采购数量',
    dataIndex: 'ABQuantity'
}];

