import React, {Component} from 'react';
import {Table, Button, Input, InputNumber, Select} from 'antd';
import {API_URL} from "../constants";
import {getCrmRecordFromSql, isBlank} from "../util/util";

const ZOHO = window.ZOHO;

class InvoiceDetail extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            packingData: props.packingData,
            packingColumn: [{
                title: '序号',
                dataIndex: 'index',
                render: (text, record, index) => index + 1, // 使用index+1生成序号
            }, {
                title: '柜数',
                dataIndex: 'containerCount',
                key: 'containerCount',
                editable: true,
                render: (text, record, index) => <InputNumber
                    step={1}
                    style={{width: '200px'}}
                    min={0}
                    value={text}
                    onChange={(e) => this.handleCellChange(e, 'containerCount', index)}
                />
            }, {
                title: '柜型',
                dataIndex: 'containerType',
                key: 'containerType',
                editable: true,
                render: (text, record, index) =>
                    <Select value={text}
                            style={{width: '200px'}}
                            onChange={(e) => this.handleCellChange(e, 'containerType', index)}>
                        <Select.Option value="LCL">LCL</Select.Option>
                        <Select.Option value="20GP">20GP</Select.Option>
                        <Select.Option value="40GP">40GP</Select.Option>
                        <Select.Option value="40HQ">40HQ</Select.Option>
                        <Select.Option value="20RH">20RH</Select.Option>
                        <Select.Option value="40HR">40HR</Select.Option>
                        <Select.Option value="By-Air">By-Air</Select.Option>
                        <Select.Option value="自提">自提</Select.Option>
                        <Select.Option value="拼箱">拼箱</Select.Option>
                        <Select.Option value="空运">空运</Select.Option>
                    </Select>
            }, {
                title: '合并发货订单号',
                dataIndex: 'mergedOrderNumber',
                key: 'mergedOrderNumber',
                editable: true,
                render: (text, record, index) => <Input
                    value={text}
                    onChange={(e) => this.handleCellChange(e.target.value, 'mergedOrderNumber', index)}
                />
            }, {
                title: '备注',
                dataIndex: 'remarks',
                key: 'remarks',
                editable: true,
                render: (text, record, index) => <Input
                    value={text}
                    onChange={(e) => this.handleCellChange(e.target.value, 'remarks', index)}
                />
            }],
            selectedRowKeys: [],
        };
    }

    componentDidMount() {

    }

    getPackingData = () => {
        const {packingData} = this.state;
        return packingData;
    }


    handleCellChange = (newValue, apiname, index) => {
        const {packingData} = this.state;
        const newData = [...packingData];
        newData[index][apiname] = newValue;
        this.setState({
            data: newData,
        });
    };
    addRow = () => {
        const {packingData} = this.state;
        const newobj = {
            ...{
                "key": packingData.length + 1,
                "mergedOrderNumber": "",
                "remarks": "",
                "containerType": "",
                "containerCount": ""
            }
        };
        const newData = [...packingData, newobj]; // 创建一个新的数组，将 initObj 添加到末尾
        this.setState({packingData: newData});
    }
    removeRow = (index) => {
        const {packingData, selectedRowKeys} = this.state;

        const newData = packingData.filter(item => !selectedRowKeys.includes(item.key)); // 创建一个新的数组，复制原始数据
        this.setState({packingData: newData, selectedRowKeys: []});
    }
    onSelectChange = (newselectedRowKeys) => {
        console.log(newselectedRowKeys)
        this.setState({selectedRowKeys: newselectedRowKeys});
    };

    render() {
        const {packingData, packingColumn, selectedRowKeys} = this.state;
        return (
            <div>

                <div style={{
                    display: 'flex',
                    justifyContent: 'flex-start',
                    marginBottom: 16,
                    marginTop: 16,
                    marginRight: 16
                }}>
                    <Button onClick={this.addRow} type="primary" style={{marginRight: 10}}>
                        新增一行
                    </Button>
                    <Button onClick={this.removeRow} type="primary" danger>删除一行</Button>
                </div>

                <Table dataSource={packingData} columns={packingColumn} rowSelection={{
                    fixed: true,
                    selectedRowKeys: selectedRowKeys, // 多选表格选中行的 keys 数组
                    onChange: this.onSelectChange, // 选中行发生变化时的回调函数
                }}
                       pagination={false}
                >
                </Table>
            </div>
        );
    }
}

export default InvoiceDetail;