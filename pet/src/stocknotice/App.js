import React, {Component} from 'react';
import QuoteForm from './Form.js';
import {message, Tabs} from 'antd';
import PackingDetails from './PackingDetails';
import StockDetails from './StockDetails';


class App extends Component {
    constructor(props) {
        super(props);
        this.formRef = React.createRef();
        this.packingRef = React.createRef();

        this.state = {
            activeKey: '1',
            packingData: [],
            FcontractNo: [], // 存储FcontractNo的值
            formData: {},
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.packingData !== this.state.packingData) {
            // FcontractNo发生变化时触发重新渲染
            this.forceUpdate();
        }
        if (prevState.FcontractNo !== this.state.FcontractNo) {
            // FcontractNo发生变化时触发重新渲染
            this.forceUpdate();
        }
    }

    onChange = (activeKey) => {
        if (activeKey === '2') {
            const formInstance = this.formRef.current;
            this.setState({activeKey, packingData: formInstance.getPackingData()}); // 更新activeKey和FcontractNo的值
        }
        if (activeKey === '3') {
            const formInstance = this.formRef.current;
            const formData = formInstance.formRef.current;
            let FcontractNo = formData.getFieldValue('FcontractNo');
            console.log(FcontractNo)
            if (FcontractNo === undefined || FcontractNo.length === 0) {
                message.error('请先选择外销合同号');
                return
            }

            this.setState({
                activeKey,
                FcontractNo,
                formData,
                packingData: this.packingRef.current === null ? [] : this.packingRef.current.getPackingData()
            }); // 更新activeKey和FcontractNo的值
        }
        this.setState({activeKey});
    };

    getPackingData = () => {
        const {packingData} = this.state;
        return packingData;
    }

    render() {
        const {activeKey, packingData, formData, FcontractNo} = this.state;
        let key = '';
        if (FcontractNo.length > 0) {
            key = FcontractNo[0].key;
        }
        return (
            <div>
                <Tabs defaultActiveKey="1" centered activeKey={activeKey} onChange={this.onChange}>
                    <Tabs.TabPane tab="主要内容" key="1">
                        <QuoteForm ref={this.formRef}/>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="装箱明细" key="2">
                        <PackingDetails packingData={packingData} ref={this.packingRef}
                        />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="备货明细" key="3">
                        <StockDetails key={key}
                                      getPackingData={this.getPackingData()}
                                      FcontractNo={FcontractNo}
                                      formData={formData}
                                      packingData={packingData}/>
                    </Tabs.TabPane>
                </Tabs>
            </div>
        );
    }
}

export default App;