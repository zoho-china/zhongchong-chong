import React, {Component} from "react";
import {Col, DatePicker, Form, Input, Row, Select, Divider, InputNumber} from "antd";
import moment from 'moment';
import {getOptions, getCrmRecordFromSql, getCrmRecordById, isBlank} from "../util/util";
import '../styles.css';
import dayjs from "dayjs"; // 引入CSS文件

const dateFormat = 'YYYY-MM-DD';

const ZOHO = window.ZOHO;
const {TextArea} = Input;
let timeout;
let currentValue;

class contractForm extends Component {

    constructor(props) {
        super(props);
        this.formRef = React.createRef();

        this.state = {
            // 客户
            accountOptions: [],
            // 合同号
            FcontractNoOptions: [],
            // 工厂
            plantOptions: [],
            // 发货人代码
            ShipperInformationOptions: [],
            // 国家
            StatesOptions: [],
            // 币种
            CurrencyOptions: [],
            FcontractNo: [],
            packingData: [],  // 目的港
            DestinationPortOptions: [],
            // 起运港
            LoadingPortOptions: [],
        };
    }

    async componentDidMount() {
        const {current} = this.formRef;

        let FcontractNoOptions = [];
        let accountOptions = [];
        let plantOptions = [];
        let ShipperInformationOptions = [];
        let CurrencyOptions = [];
        let StatesOptions = [];
        try {
            let Currencytypes = await getOptions("Currencytype");
            CurrencyOptions = Currencytypes.map(item => ({
                value: item.id,
                label: item.Name,
            }));
            let accounts = await getOptions("Accounts");
            accountOptions = accounts.map(item => ({
                value: item.id,
                label: item.Account_Name,
            }));
            let Plants = await getOptions("Plant");
            plantOptions = Plants.map(item => ({
                value: item.id,
                label: item.Name,
            }));
            let Foreignsacontracts = await getOptions("Foreignsacontract");
            FcontractNoOptions = Foreignsacontracts.map(item => ({
                value: item.id,
                label: item.Name,
            }));
            let States = await getOptions("States");
            StatesOptions = States.map(item => ({
                value: item.id,
                label: item.Name,
            }));
            let ShipperInformations = await getOptions("ShipperInformation");
            ShipperInformationOptions = ShipperInformations.map(item => ({
                value: item.id,
                label: item.Name,
            }));


            current.setFieldValue("Documentdate", dayjs(moment().format(dateFormat), 'YYYY-MM-DD'));
            current.setFieldValue("Exchangerate", 1)

        } catch (error) {
            console.error(error);
        }
        let FcontractNo = [];
        if (window.id !== undefined) {
            try {

                const formdata = await getCrmRecordById("PSNotice", window.id);
                formdata.ContractDate = isBlank(formdata.ContractDate) ? undefined : moment(formdata.ContractDate);
                formdata.Deliverytime = isBlank(formdata.Deliverytime) ? undefined : moment(formdata.Deliverytime);

                formdata.Currencys = {
                    value: formdata.Currencys?.id,
                    label: formdata.Currencys?.name,
                };
                formdata.Account = {
                    value: formdata.Account?.id,
                    label: formdata.Account?.name,
                }
                formdata.Plant = {
                    value: formdata.Plant?.id,
                    label: formdata.Plant?.name,
                }
                formdata.ShipperInformation = {
                    value: formdata.ShipperInformation?.id,
                    label: formdata.ShipperInformation?.name,
                }
                formdata.States = {
                    value: formdata.States?.id,
                    label: formdata.States?.name,
                }
                formdata.FcontractNo?.map(item => {
                    item.value = item.field1.id
                    item.label = item.field1.name
                    item.key = item.field1.id
                    delete item.field1;

                })
                FcontractNo = formdata.FcontractNo
                current.setFieldsValue(formdata);
                console.log(formdata)
            } catch (error) {
                console.error(error);
            }
        }
        console.log(FcontractNo)
        this.setState({ // 客户
            accountOptions: accountOptions,
            plantOptions: plantOptions,
            ShipperInformationOptions: ShipperInformationOptions,
            FcontractNoOptions: FcontractNoOptions,
            CurrencyOptions: CurrencyOptions,
            StatesOptions: StatesOptions,
            FcontractNo: FcontractNo
        });
    }


    changeFcontractNo = (values) => {
        const {current} = this.formRef;
        current.setFieldValue("FcontractNo", values);
        console.log(values)
        if (values.length === 0) {
            current.setFieldValue("Account", {
                value: "",
                label: "",
            });
            current.setFieldValue("AccountName", "");
            current.setFieldValue("AccountOrderNo", "");
            current.setFieldValue("ShipperInformation", {
                value: "",
                label: "",
            });
            current.setFieldValue("ShipperInformationName", "");
            current.setFieldValue("ContractDate", "");
            current.setFieldValue("Currencys", {
                value: "",
                label: "",
            });
            current.setFieldValue("States", {
                value: "",
                label: "",
            });
            current.setFieldValue("Exchangerate", "1");
            current.setFieldValue("ShippingPort", {
                value: "",
                label: "",
            });
            current.setFieldValue("DestinationPort", {
                value: "",
                label: "",
            });
            this.setState({ // 客户
                FcontractNo: values
            });
            return
        }
        ZOHO.CRM.API.getRecord({
            Entity: "Foreignsacontract", approved: "both", RecordID: values[0]?.key
        }).then(data => {
            current.setFieldValue("AccountName", data.data[0].Account?.name);

            current.setFieldValue("Account", {
                value: data.data[0].Account?.id,
                label: data.data[0].Account?.name,
            });
            current.setFieldValue("AccountOrderNo", data.data[0].AccountOrderNo);
            current.setFieldValue("ShipperInformation", {
                value: data.data[0].Header?.id,
                label: data.data[0].Header?.name,
            });
            current.setFieldValue("ShipperInformationName", data.data[0].ShipperInformationName);
            current.setFieldValue("ContractDate", moment(data.data[0].ContractDate));
            current.setFieldValue("ProcurementTerms", data.data[0].PayTerms);
            this.changeCurrencys({
                key: data.data[0].Currencys?.id,
                label: data.data[0].Currencys?.name
            })
            current.setFieldValue("ShippingPort", {
                value: data.data[0].ShippingPort?.id,
                label: data.data[0].ShippingPort?.name,
            });
            current.setFieldValue("DestinationPort", {
                value: data.data[0].DestinationPort?.id,
                label: data.data[0].DestinationPort?.name,
            });
        })
        this.setState({ // 客户
            FcontractNo: values
        });
    }
    changeAccount = async (value) => {
        const {current} = this.formRef;
        current.setFieldValue("Account", {
            value: value.key,
            label: value.label
        })
    }
    changePlant = (value) => {
        const {current} = this.formRef;
        current.setFieldValue("Plant", {
            value: value.key,
            label: value.label
        })
        ZOHO.CRM.API.getRecord({
            Entity: "Plant", approved: "both", RecordID: value.key
        }).then(data => {
            current.setFieldValue("PlantName", data.data[0].FullName)
        })
    }
    changeShipperInformation = (value) => {
        const {current} = this.formRef;
        current.setFieldValue("ShipperInformation", {
            value: value.key,
            label: value.label
        })
        ZOHO.CRM.API.getRecord({
            Entity: "ShipperInformation", approved: "both", RecordID: value.key
        }).then(data => {
            current.setFieldValue("ShipperInformationName", data.data[0].China_Name)
        })
    }
    changeStates = (value) => {
        const {current} = this.formRef;
        current.setFieldValue("States", {
            value: value.key,
            label: value.label
        })
    }
    changeCurrencys = (value) => {
        const {current} = this.formRef;
        if (value === undefined) {
            current.setFieldValue("Currencys", {
                value: '',
                label: ''
            })
            current.setFieldValue("Exchangerate", 1)
        } else {
            current.setFieldValue("Currencys", {
                value: value.key,
                label: value.label
            })
            ZOHO.CRM.API.getRecord({
                Entity: "Currencytype", approved: "both", RecordID: value.key
            }).then(data => {
                current.setFieldValue("Exchangerate", data.data[0].Exchangerates)
            })
        }
    }


    handleSearch = (value) => {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
        currentValue = value;
        const fake = () => {
            getCrmRecordFromSql(`select id,Name from Foreignsacontract where Name like '%${value}%'`).then(result => {
                let FcontractNoOptions = result.map((item) => ({
                    value: item.id,
                    label: item.Name,
                }));
                this.setState({FcontractNoOptions: FcontractNoOptions});
            })
        };
        if (value) {
            timeout = setTimeout(fake, 300);
        }
    }

    handleCancel = () => {
        const {current} = this.formRef;
        current.resetFields();
    };

    getPackingData = () => {
        const {packingData} = this.state;
        return packingData;
    }

    render() {
        const {
            accountOptions,
            FcontractNoOptions,
            ShipperInformationOptions,
            CurrencyOptions,
            FcontractNo,
            DestinationPortOptions,
            LoadingPortOptions,
        } = this.state;
        const {Item} = Form;
        return (
            <div className="body">
                <Form ref={this.formRef}>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="编号" name="Name" labelAlign="right"
                                  rules={[{required: true, message: '请输入生产通知单号'}]}>
                                <Input className="input-field"/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="外销合同" name="FcontractNo" labelAlign="right">
                                <Select
                                    mode="multiple"
                                    labelInValue
                                    className="input-field"
                                    showSearch
                                    onChange={this.changeFcontractNo}
                                    options={FcontractNoOptions}
                                    optionLabelProp="label"
                                    filterOption={false}
                                    onSearch={this.handleSearch}
                                    value={FcontractNo}
                                /> </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="货好时间" name="Deliverytime">
                                <DatePicker className="input-field" format={dateFormat}/>
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="客户代码" name="Account" labelAlign="right"
                                  rules={[{required: true, message: '请选择客户'}]}>
                                <Select
                                    labelInValue
                                    className="input-field"
                                    showSearch
                                    onChange={this.changeAccount}
                                    options={accountOptions}
                                    optionLabelProp="label"
                                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                />
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="客户名称" name="AccountName">
                                <Input className="input-field" disabled={true}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="币种" name="Currencys" labelAlign="right">
                                <Select
                                    labelInValue
                                    className="input-field"
                                    showSearch
                                    onChange={this.changeCurrencys}
                                    options={CurrencyOptions}
                                    optionLabelProp="label"
                                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                />
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="发货人代码" name="ShipperInformation">
                                <Select
                                    labelInValue
                                    className="input-field"
                                    showSearch
                                    onChange={this.changeShipperInformation}
                                    options={ShipperInformationOptions}
                                    optionLabelProp="label"
                                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                />
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="发货人名称" name="ShipperInformationName">
                                <Input className="input-field" disabled={true}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="汇率" name="Exchangerate">
                                <InputNumber className="input-field" step={0.01}/>
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="装运港" name="ShippingPort">
                                <Select
                                    className="input-field"
                                    labelInValue
                                    showSearch
                                    onChange={value => {
                                        this.changePort('LoadingPort', value)
                                    }}
                                    options={LoadingPortOptions}
                                    optionLabelProp="label"
                                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                />
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="目的港" name="DestinationPort">
                                <Select
                                    className="input-field"
                                    labelInValue
                                    showSearch
                                    onChange={value => {
                                        this.changePort('DestinationPort', value)
                                    }}
                                    options={DestinationPortOptions}
                                    optionLabelProp="label"
                                    filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                />
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="单据日期" name="Documentdate">
                                <DatePicker className="input-field" format={dateFormat}/>
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="唛头" name="ShippingMark">
                                <Input className="input-field"/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="预计船期" name="EstimatedShippingDate">
                                <DatePicker className="input-field" format={dateFormat}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="代理" name="agency" labelAlign="right">
                                <Input className="input-field"/>
                            </Item>
                        </Col>
                    </Row>

                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="提单类型" name="billtype">
                                <Select className="input-field">
                                    <Select.Option value="电放">电放</Select.Option>
                                    <Select.Option value="正本提单">正本提单</Select.Option>
                                    <Select.Option value="无">无</Select.Option>
                                </Select>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="产品分类" name="productCategory" labelAlign="right">
                                <Input className="input-field"/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="提单特殊要求" name="specialRequest" labelAlign="right">
                                <Input className="input-field"/>
                            </Item>
                        </Col>

                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="客户订单号" name="AccountOrderNo">
                                <Input className="input-field"/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="调整报关金额" name="adjustedCustomsAmount">
                                <Input className="input-field"/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="装货地点" name="pickupLocation">
                                <Input className="input-field"/>
                            </Item>
                        </Col>

                    </Row>

                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="等通知放货" name="onHoldForRelease">
                                <Select className="input-field">
                                    <Select.Option value="等通知">等通知</Select.Option>
                                    <Select.Option value="可放单">可放单</Select.Option>
                                </Select> </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="等通知放单" name="Termsandconditions">
                                <Select className="input-field">
                                    <Select.Option value="等通知">等通知</Select.Option>
                                    <Select.Option value="可放单">可放单</Select.Option>
                                </Select>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="采购条款" name="ProcurementTerms">
                                <TextArea className="input-field" rows={4}/> </Item>
                        </Col>
                    </Row>

                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="出口数量" name="exportQuantity">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="总数量" name="totalQuantity">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="总件数" name="totalPieces">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="总体积" name="TotalVolume">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="总净重" name="TotalNetWeight">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="总毛重" name="TotalGrossWeight">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="总金额" name="totalAmount">
                                <InputNumber className="input-field" step={1}/>
                            </Item>
                        </Col>
                    </Row>
                    <Divider orientation="left">提单信息</Divider>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="提单收货人" name="receiver">
                                <TextArea className="input-field" rows={4}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="客户发票抬头" name="customerInvoiceTitle">
                                <TextArea className="input-field" rows={4}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="提单通知人" name="notifier">
                                <TextArea className="input-field" rows={4}/>
                            </Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Item label="提单发货人" name="shipper">
                                <TextArea className="input-field" rows={4}/>
                            </Item>
                        </Col>
                        <Col span={8}>
                            <Item label="进仓日期" name="storageDate">
                                <DatePicker className="input-field" format={dateFormat}/>
                            </Item>
                        </Col>
                    </Row>
                    <Divider orientation="left">条款内容</Divider>
                    <Item label="装箱单后置条款(打印在商品明细后面)" name="shipmentTerms">
                        <TextArea/> </Item>
                    <Item label="发票/箱单前置条款(打印在商品明细前面)" name="shippingDocumentTerms">
                        <TextArea/> </Item>
                    <Item label="发票后置条款(打印在商品明细后面)" name="invoiceTerms">
                        <TextArea/> </Item>
                </Form>

            </div>

        );
    }
}

export default contractForm;