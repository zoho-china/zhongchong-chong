import React, {Component} from 'react';
import {Table, Button, Input, message, InputNumber} from 'antd';
import {API_URL, contractColumn, invoiceColumn} from "../constants";
import {getCrmRecordFromSql, isBlank} from "../util/util";
import StockTable from './StockTable'

const ZOHO = window.ZOHO;

class InvoiceDetail extends Component {
    constructor(props) {
        super(props);
        this.stockTableRef = React.createRef()
        console.log(props)
        this.state = {
            contractData: [],
            contractColumn: contractColumn,
            selectedRows: [],
            invoiceDatas: [],
            packingData: props.packingData,
            scrollWidth: contractColumn.length * 200,
            fcontractNo: props.FcontractNo,
            selectedData: [],
            formData: props.formData,
        };
    }

    componentDidMount() {
        const {fcontractNo} = this.state
        if (!isBlank(fcontractNo)) {
            let fcontractNoID = fcontractNo.map(item => item.key);
            let contractDatas = [];
            let apinames = contractColumn.map(column => column.dataIndex);
            getCrmRecordFromSql(`select ${apinames} from Foreignsacontractdetail where Foreignsacontract.id in (${fcontractNoID})`).then(data => {
                data?.forEach(item => {
                    let contractData = {};
                    contractColumn.forEach(column => {
                        contractData[column.dataIndex] = item[column.dataIndex];
                    })
                    contractData['key'] = item['id']
                    contractDatas.push(contractData)
                })
                this.setState({contractData: contractDatas})
            });
        }
    }


    handleCellChange = (newValue, field, index) => {
        const {invoiceDatas} = this.state;

        const newData = [...invoiceDatas];
        if (field.data_type === "text" || field.data_type === "double" || field.data_type === "integer" || field.data_type === "textarea") {
            newData[index]['Name'] = newValue;
        }

        if (field.data_type === "lookup") {
            newData[index][field.api_name] = {
                id: newValue.value,
                name: newValue.label
            }
        }

        this.setState({
            data: newData,
        });
    };

    handleTransfer = () => {
        const {contractData, selectedRows} = this.state;
        const updatedData = contractData.filter((item) => selectedRows.includes(item.key));
        const unselectedData = contractData.filter((item) => !selectedRows.includes(item.key));
        let invoiceDatas = this.stockTableRef.current.getInvoiceDatas();

        console.log(updatedData)
        let datas = [];
        updatedData.forEach((item, index) => {
            let stockData = {
                Name: invoiceDatas.length + index + 1,
                OrderNo: item.AccountOrderNo,
                Product: item['Product.Product_Name'],
                ProductId: item['Product.id'],
                AccountprtNo: item.AccountprtNo,
                internalcode: item.internalcode,
                Englishname: item.Englishname,
                Specification: item.Specification,
                Brand: item.Brand,
                Unit_Price: item.Unit_Price,
                Unit: item.Unit,

                NumberofCartons: item.NumberofCartons,
                Lengthofouterbox: item.Lengthofouterbox,
                Heightofouterbox: item.Heightofouterbox,
                Cartonwidth: item.Cartonwidth,
                QuantityperCarton: item.QuantityperCarton,
                Netweight: item.Netweight,
                Volume: item.Volume,
                TotalVolume: item.TotalVolume,
                TotalGrossweight: item.TotalGrossWeight,
                ChinesePackagedesp: item.ChinesePackagedesp,
                Chptrequirements_material: item.Chptrequirements_material,

                Productname: item.Productname,
                Chinesespecification: item.Chinesespecification,
                Chineseingredients: item.Chineseingredients,
                Nuofboxesinpg: item.Nuofboxesinpg,
                Nuofboxeinnerbox: item.Nuofboxeinnerbox,

                Grossweight: item.Grossweight,
                Packaginunit: item.Packaginunit,
                // Packaginunit_id: item['Packaginunit.id'],

                TotalNetweight: item.TotalNetWeight,
                Foreignsacontract: item['Foreignsacontract.Name'],
                key: item.key,
                MaxQuantity: item.ABQuantity,

                Purchaseunitprice: item.Unit_Price,
                Qty: item.ABQuantity,
            };
            datas.push(stockData)
        })
        console.log(datas)
        invoiceDatas = invoiceDatas.concat(datas);
        this.setState({
            selectedData: updatedData,
            invoiceDatas: invoiceDatas,
            contractData: unselectedData,
            selectedRows: [],
        }, () => {
            // 在setState的回调函数中，将更新后的invoiceDatas传递给子组件
            this.stockTableRef.current.updateInvoiceDatas(invoiceDatas, updatedData);
        });
    };

    updatecontractData = (newData) => {
        const {contractData} = this.state

        let a = [...contractData]
        newData.forEach(item => {
            a.push(item)
        })
        this.setState({
            contractData: a,
            selectedRows: []
        });
    }


    handleCancel = () => {
        // 处理取消逻辑
        // ...
        console.log('取消');
    };
    handleSave = () => {
        const {formData} = this.state
        formData.validateFields().then((values) => {
            values.Deliverytime = values.Deliverytime?.format('YYYY-MM-DD');
            values.Documentdate = values.Documentdate?.format('YYYY-MM-DD');
            values.EstimatedShippingDate = values.EstimatedShippingDate?.format('YYYY-MM-DD');
            values.storageDate = values.storageDate?.format('YYYY-MM-DD');
            values.Account = {
                id: values.Account?.value,
                name: values.Account?.label
            }
            values.Currencys = {
                id: values.Currencys?.value,
                name: values.Currencys?.label
            }
            values.ShippingPort = {
                id: values.ShippingPort?.value,
                name: values.ShippingPort?.label
            }
            values.ShipperInformation = {
                id: values.ShipperInformation?.value,
                name: values.ShipperInformation?.label
            }
            values.DestinationPort = {
                id: values.DestinationPort?.value,
                name: values.DestinationPort?.label
            }
            values.FcontractNo?.map(item => {
                item.field1 = {
                    id: item.key
                }
            })

            values.LinkingModule4 = this.props.getPackingData;
            if (values.LinkingModule4.length <= 0) {
                message.error("请添加装箱明细");
                return;
            }
            let req_data = {
                "headers": {},
                "url": API_URL + `/crm/v3/Stocknotice`,
                "param_type": 2
            };
            if (window.id !== undefined) {
                req_data.method = "PUT";
                values.id = window.id;
            } else {
                req_data.method = "POST";
            }
            req_data.parameters = {
                data: [values]
            }
            let invoiceData = this.stockTableRef.current.state.data;
            ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                let id = data.details.statusMessage.data[0].details.id;
                console.log(id)
                console.log(invoiceData)
                let contractQtys = [];
                invoiceData.map(item => {
                    item.Stocknotice = {
                        id: id,
                        name: values.Name
                    }
                    if (window.id === undefined) {
                        item.Product = {
                            id: item.ProductId,
                            Product_Name: item.Productname,
                        }
                        item.Foreignsacontractdetail = {
                            id: item.key,
                            Name: item.Name
                        }
                    }
                    item.Name = item.Name + "";
                    if (isBlank(item.id)) {
                        delete item.id;
                    }
                    contractQtys.push({
                        "id": item.key,
                        "ABQuantity": item.Quantity,
                    })
                })
                let req_data = {
                    "parameters": {
                        data: invoiceData
                    },
                    "headers": {},
                    "url": API_URL + `/crm/v3/Stocknoticedetail`,
                    "param_type": 2
                };
                if (window.id !== undefined) {
                    req_data.method = "PUT";
                } else {
                    req_data.method = "POST";
                }
                ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                    console.log(data);
                });
                message.success("保存成功");
                ZOHO.CRM.UI.Record.open({Entity: "Stocknotice", RecordID: id})
                    .then(function (data) {
                        console.log(data)
                        req_data = {
                            "parameters": {
                                data: contractQtys
                            },
                            "headers": {},
                            "method": "PUT",
                            "url": API_URL + `/crm/v3/Foreignsacontractdetail`,
                            "param_type": 2
                        };
                        ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                            console.log(data);
                        });
                    })
            });
        }).catch(e => {
            console.log(e)
            e.errorFields.forEach(errors => {
                message.error(errors.errors)
            })
        })

    };

    onRowChange = (selectedRows) => {
        console.log(selectedRows)
        this.setState({selectedRows});
    }

    render() {
        const {contractData, selectedRows, invoiceDatas, contractColumn, scrollWidth, selectedData} = this.state;


        return (
            <div>
                <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 16}}>
                    <Button type="primary" onClick={this.handleSave}>
                        保存
                    </Button>
                    <Button style={{marginLeft: 8}} onClick={this.handleCancel}>
                        取消
                    </Button>
                </div>
                <div>
                    <StockTable ref={this.stockTableRef}
                                dataSource={invoiceDatas}
                                selectedData={selectedData}
                                moduleName="Stocknoticedetail"
                                parentModuleName="Stocknotice"
                                updatecontractData={this.updatecontractData}
                                paste={false}/>

                </div>
                <div>
                    <div style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        marginBottom: 16,
                        marginTop: 16,
                        marginRight: 16
                    }}>
                        <Button type="primary" onClick={this.handleTransfer}>
                            确定
                        </Button>
                    </div>
                    <Table dataSource={contractData} columns={contractColumn} rowSelection={{
                        fixed: true,
                        selectedRowKeys: selectedRows, // 多选表格选中行的 keys 数组
                        onChange: this.onRowChange, // 选中行发生变化时的回调函数
                    }}
                           scroll={{y: 240, x: scrollWidth}}>

                    </Table>
                </div>

            </div>
        );
    }
}

export default InvoiceDetail;