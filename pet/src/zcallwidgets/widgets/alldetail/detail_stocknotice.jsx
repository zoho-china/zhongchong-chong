import React, { useEffect, useCallback, useState, useMemo } from 'react';
import { CircularProgress, Box, Button, Drawer, Stack, IconButton, TextField, InputAdornment, Grid } from '@mui/material';
import { MaterialReactTable, useMaterialReactTable, MRT_ShowHideColumnsButton as ShowHideColumnsButton } from 'material-react-table';
import { getCrmRecordById,getCrmRecordFromSql, getCrmvariables } from "../../util/util";
import { Clear, CopyAll, GetAppOutlined, Search } from '@mui/icons-material';
import { useSnackbar } from '../customized/SnackbarProvider'; // 导入自定义的 hook
import exportToExcel from '../../util/exportToExcel';
import { MRT_Localization_ZH_HANS as LocalizationZH } from 'material-react-table/locales/zh-Hans';


//备货通知明细操作

const App = (pagedata) => {
    const [tabledata, setTabledata] = useState([]); // 用于存储获取的数据
    const [isloacked, setIsloacked] = useState(false); // 数据锁定状态
    const [crmdata, setCrmdata] = useState([]); // 用于存储CRM获取的数据
    const [sodetaildata, setSodetaildata] = useState([]); // 用于存储获取的数据
    const [productdata, setProductdata] = useState([]); // 用于存储获取的数据
    // const [deletedata, setDeletedata] = useState([]); // 用于存储删除的数据保存
    const [rowSelection, setRowSelection] = useState([]); // 用于存储选中的行
    // const [rowSelection_product, setRowSelection_product] = useState([]); // 用于存储产品选择其选中的行
    const [loading, setLoading] = useState(true); // 用于控制加载状态
    // const [loadingptable, setLoadingptable] = useState(true); // 用于控制加载状态
    const [openDrawer_sodetail, setOpenDrawer_sodetail] = useState(false); // 控制合同明细抽屉的显示与隐藏
    const [openDrawer_product, setOpenDrawer_product] = useState(false); // 控制产品明细抽屉的显示与隐藏合同明细抽屉的显示与隐藏
    const [searchValue, setSearchValue] = useState(''); // 用于存储输入框的值
    const [canEdit, setCanEdit] = useState(false); // 控制是否可以编辑

    const openSnackbar = useSnackbar();


    const fetchData = useCallback(async () => {
        try {
            setLoading(true); // 设置加载状态为true
            let response = await getCrmvariables("exstuserids");
            let usersex = [];
            if (
                response &&
                response.details &&
                response.details.statusMessage &&
                response.details.statusMessage.variables &&
                Array.isArray(response.details.statusMessage.variables) &&
                response.details.statusMessage.variables.length > 0
            ) {
                let result = response?.details?.statusMessage?.variables[0]?.value;
                console.log("result", result);
                usersex = result.split(",");
            }

            // console.log("usersex", usersex);
            let currentuserinfo = await window.ZOHO.CRM.CONFIG.getCurrentUser();
            // console.log(currentuserinfo);
            // let usersex = ['185647000000267001', '185647000006305201','185647000001055001'];
            if (currentuserinfo?.users?.length > 0 && usersex.includes(currentuserinfo?.users[0]?.id)) {
                setCanEdit(true);
            }
            // console.log("pagedata", pagedata)
            const dataid = pagedata?.data?.EntityId[0];
            const entity = pagedata?.data?.Entity;
            const data = await getCrmRecordById(entity, dataid);
            // console.log("data", data);
            if (data) {
                setCrmdata(data); // 将获取的数据存储到crmdata中
                let isloacked = data?.Locked__s || false;
                setIsloacked(isloacked);
                if (data?.LinkingModule9) {
                    let detaildata = data?.LinkingModule9;
                    console.log("detaildata", detaildata);
                    const filteredData = detaildata.filter(item =>
                        item?.Product !== null &&
                        item?.Product?.id !== null
                    ); // 过滤掉 Product_Name 为 null 或 id 为 null 的数据
                    setTabledata(filteredData); // 将获取的数据存储到tabledata中
                }
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setLoading(false); // 数据获取完成后，将加载状态设置为false
        }
    }, [pagedata]);



    useEffect(() => {
        window.ZOHO.CRM.UI.Resize({ height: "100%", width: "100%" })
        fetchData();
    }, [fetchData]);




    //获取商品库
    const productsGet = async (searchQuery = "") => {
        setLoading(true);
        try {
            // let account = crmdata?.Account_Name?.id;
            // console.log("account", account);
            let sql = `select id,Product_Name,Account.Account_Name as acname,AccountprtNo,ReferenceMaterialNumber1,ReferenceMaterialNumber2,Englishname,Unit_Price,Specification,Productname,Chinesespecification,Category,Classify1,Classify2,internalcode,Lengthofouterbox,Cartonwidth,Heightofouterbox,Volume,Netweightofinnebox,Nuofboxesinpg,Nuofboxeinnerbox,Packagingquantityperpg,Netweight,Grossweight,Unit,Chptrequirements_material,Brand,Packaginunit,Declarationelements,HSCode_text 
      from Products where field1 = '关务部部长审批通过' and Category = '费用类' limit 2000`;
            if (searchQuery) {
                searchQuery = searchQuery.trim();
                sql = `select id,Product_Name,Account.Account_Name as acname,AccountprtNo,ReferenceMaterialNumber1,ReferenceMaterialNumber2,Englishname,Unit_Price,Specification,Productname,Chinesespecification,Category,Classify1,Classify2,internalcode,Lengthofouterbox,Cartonwidth,Heightofouterbox,Volume,Netweightofinnebox,Nuofboxesinpg,Nuofboxeinnerbox,Packagingquantityperpg,Netweight,Grossweight,Unit,Chptrequirements_material,Brand,Packaginunit,Declarationelements,HSCode_text 
        from Products where field1 = '关务部部长审批通过' and (Product_Name like '%${searchQuery}%' or AccountprtNo like '%${searchQuery}%') limit 2000`;
            }
            let productresult = await getCrmRecordFromSql(sql);
            setProductdata(productresult);
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setLoading(false);
        }
    };
    //获取合同明细
    const sodetailGet = async () => {
        setLoading(true);
        try {
            let soids = crmdata?.id1; // 获取订单ID
            let soarray = soids.split(",");
            let idstring = soarray.map(item => `'${item}'`).join(', ');
            //, Chineseingredients
            let sql = `select Parent_Id.id as pid, Parent_Id.AccountOrderNo as AccountOrderNo, Parent_Id.Subject as Subject,HSCode, ChinesePackagedesp, Productname, Chptrequirements_material, Chinesespecification, Volume, Nuofboxesinpg, Nuofboxeinnerbox, internalcode, Netweight, Packaginunit, List_Price, Unit, Brand, ShippingMark, field3, Product_Name.id as pdid, Product_Name.Product_Name as pname,Product_Name.Overseasfactorypurchaseprice as Purchaseunitprice, AccountprtNo, Cartonwidth, field, field2, field1, ItemDiscountRate,Net_Total, Description, Quantity, PSQty, SNQty, Packagingquantityperpg, Grossweight,Discount, W, NumberofCartons, Category, Englishname, Specification, Total, Lengthofouterbox, Heightofouterbox, id,WItemId from Ordered_Items where Parent_Id in (${idstring}) limit 2000`;
            let sodetailresult = await getCrmRecordFromSql(sql);

            //可用数量为0
            //已被选择
            sodetailresult.forEach(item => {
                const foundItem = tabledata.find(i => i.id1 === item.id);
                if (foundItem) {
                    item.selected = true;
                }
            });

            setSodetaildata(sodetailresult);
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleSave = async () => {
        console.log("tabledata", table.getRowModel().rows);
        // return;
        // console.info(table.getRowModel().rows); //example - get access to all page rows in the table
        // console.info(table.getSelectedRowModel()); //example - get access to all selected rows in the table
        // console.info(table.getState().sorting); //example - get access to the current sorting state without having to manage it yourself
        setLoading(true); // 显示加载状态
        try {
            // const dataid = crmdata.id;
            const dataname = crmdata.Name;
            // const olddetaildata = crmdata?.Ordered_Items;
            let requiredarray = []; //记录必填项数组
            let errorRows = [];  //记录箱数不为整数的行
            tabledata.forEach((item, index) => {
                delete item.Sequence_Number;
                delete item.LinkingModule9_Serial_Number;
                if (item.id && String(item.id).includes('new')) {
                    delete item.id;
                }
                let qty = item?.Qty || 0;
                let numberofCartons = item?.NumberofCartons || 0;
                if (qty <= 0 || numberofCartons <= 0) {
                    requiredarray.push(index + 1); // Store 1-based index
                }
                if (numberofCartons % 1 !== 0) {
                    errorRows.push(index + 1); // Store 1-based index
                }
            });
            // console.log('requiredarray',requiredarray);
            if (requiredarray.length > 0) {   //箱数可以为0，去除验证
                // openSnackbar(`第${requiredarray.join(', ')}行箱数、数量不能为空或者0！`, 'error');
                // return;
            };
            if (errorRows.length > 0) {
                openSnackbar(`第${errorRows.join(', ')}行箱数不是整数！`, 'error');
                return;
            };
            let datainfojson = {
                "Name": dataname,
                // "id": dataid,
                "LinkingModule9": tabledata,
            };
            console.log("datainfojson", JSON.stringify(datainfojson));
            let result_save = await window.ZOHO.CRM.API.upsertRecord({ Entity: "Stocknotice", APIData: datainfojson, duplicate_check_fields: ["Name"], Trigger: ["workflow"] });
            console.log(JSON.stringify(result_save));
            await window.ZOHO.CRM.UI.Popup.closeReload();
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setLoading(false);  // 隐藏加载状态
        }
    };


    //产品选择器的列配置
    const columns_product = [
        { accessorKey: 'Product_Name', header: '商品编码', minSize: 100 },
        { accessorKey: 'AccountprtNo', header: '客户货号', minSize: 70 },
        { accessorKey: 'Englishname', header: '英文品名', minSize: 100 },
        { accessorKey: 'Specification', header: '英文规格', minSize: 100 },
        { accessorKey: 'Productname', header: '中文品名', minSize: 100 },
        { accessorKey: 'Chinesespecification', header: '中文规格', minSize: 100 },
        { accessorKey: 'Category', header: '一级类别', minSize: 100 },
        { accessorKey: 'Classify1', header: '二级分类', minSize: 100 },
        { accessorKey: 'Classify2', header: '三级分类', minSize: 100 },
        { accessorKey: 'ReferenceMaterialNumber1', header: '参考料号1', minSize: 100 },
        { accessorKey: 'ReferenceMaterialNumber2', header: '参考料号2', minSize: 100 },
        { accessorKey: 'internalcode', header: '内部货号', minSize: 100 },
        { accessorKey: 'Lengthofouterbox', header: '外箱长(CM)', minSize: 100 },
        { accessorKey: 'Cartonwidth', header: '外箱宽(CM)', minSize: 100 },
        { accessorKey: 'Heightofouterbox', header: '外箱高(CM)', minSize: 100 },
        { accessorKey: 'Volume', header: '外箱体积（CBM）', minSize: 100 },
        { accessorKey: 'Netweightofinnebox', header: '单净重(G)', minSize: 100 },
        { accessorKey: 'Nuofboxesinpg', header: '包装内盒入数', minSize: 100 },
        { accessorKey: 'Nuofboxeinnerbox', header: '包装内盒数', minSize: 100 },
        { accessorKey: 'Packagingquantityperpg', header: '每包装量', minSize: 100 },
        { accessorKey: 'Netweight', header: '外箱净重(KG)', minSize: 100 },
        { accessorKey: 'Grossweight', header: '外箱毛重(KG)', minSize: 100 },
        { accessorKey: 'Unit', header: '单位', minSize: 100 },
        { accessorKey: 'Chptrequirements_material', header: '中文生产要求(材质)', minSize: 100 },
        { accessorKey: 'Brand', header: '品牌报关', minSize: 100 },
        { accessorKey: 'Packaginunit', header: '包装单位', minSize: 100 },
        { accessorKey: 'Declarationelements', header: '申报要素', minSize: 100 },
        { accessorKey: 'HSCode_text', header: 'HSCode', minSize: 100 },
        // { accessorKey: 'acname', header: '客户', minSize: 100 },
    ];

    //合同明细的列配置
    const columns_sodetail = [
        { accessorKey: 'pname', header: '商品库名称', minSize: 140, size: 140, enableEditing: false, },
        { accessorKey: 'AccountprtNo', header: '客户货号', minSize: 110, size: 110, enableEditing: false, },
        { accessorKey: 'Subject', header: '订单号', minSize: 110, size: 110, enableEditing: false, },
        { accessorKey: 'AccountOrderNo', header: '客户订单号', minSize: 140, size: 140, enableEditing: false, },
        { accessorKey: 'ShippingMark', header: '唛头', minSize: 80, size: 80, enableEditing: false, },
        { accessorKey: 'internalcode', header: '内部代码', minSize: 110, size: 110, enableEditing: false, },
        { accessorKey: 'Englishname', header: '英文品名', minSize: 100, enableEditing: false, },
        { accessorKey: 'Specification', header: '英文规格', minSize: 100, enableEditing: false, },
        { accessorKey: 'List_Price', header: '单价', minSize: 90, size: 90, enableEditing: false, },
        { accessorKey: 'Discount', header: '折扣', minSize: 90, size: 90, enableEditing: false, },
        { accessorKey: 'PSQty', header: '已生产数量', minSize: 130, size: 130, enableEditing: false, },
        { accessorKey: 'SNQty', header: '已备货数量', minSize: 130, size: 130, enableEditing: false, },
        { accessorKey: 'Quantity', header: '数量', minSize: 90, size: 90, enableEditing: false, },
        { accessorKey: 'NumberofCartons', header: '箱数', minSize: 90, size: 90, },
        { accessorKey: 'Packagingquantityperpg', header: '每箱装量', minSize: 90, size: 90, },
        { accessorKey: 'Total', header: '金额', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Net_Total', header: '总计', minSize: 100, size: 100, enableEditing: false, },
        // { accessorKey: 'DomesticOrderAmount', header: '国内接单金额', minSize: 160, size: 160, },
        { accessorKey: 'Unit', header: '单位', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Brand', header: '品牌', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Nuofboxesinpg', header: '入数', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Nuofboxeinnerbox', header: '内盒数', minSize: 100, size: 70, enableEditing: false, },
        { accessorKey: 'Lengthofouterbox', header: '长(CM)', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Cartonwidth', header: '宽(CM)', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Heightofouterbox', header: '高(CM)', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Volume', header: '体积', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Netweight', header: '净重(KG)', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Grossweight', header: '毛重(KG)', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'field2', header: '总净重', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'field1', header: '总毛重', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'field', header: '总体积', minSize: 100, size: 100, enableEditing: false, },
        { accessorKey: 'Productname', header: '中文品名', minSize: 100, enableEditing: false, },
        { accessorKey: 'ChinesePackagedesp', header: '中文包装描述', minSize: 100, enableEditing: false, },
        { accessorKey: 'Chinesespecification', header: '中文规格', minSize: 100, enableEditing: false, },
        // { accessorKey: 'Chineseingredients', header: '中文成份', minSize: 100, enableEditing: false, },
        { accessorKey: 'Chptrequirements_material', header: '中文生产要求(材质)', minSize: 180, enableEditing: false, },
        { accessorKey: 'Category', header: '类别', minSize: 90, size: 90, enableEditing: false, },
        { accessorKey: 'Packaginunit', header: '包装单位', minSize: 90, size: 90, enableEditing: false, },
        { accessorKey: 'HSCode', header: 'HS编码', minSize: 100, enableEditing: false, },
        { accessorKey: 'W', header: '生产发货通知单号', minSize: 100, enableEditing: false, },
        { accessorKey: 'TotalVolume3', header: '商品备注', minSize: 100, enableEditing: false, },
    ];

    const totalTotalGrossWeight = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.TotalGrossWeight || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );

    const totalNumberofCartons = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.NumberofCartons || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );

    const totalTotalAmount = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.TotalAmount || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );

    const totalTotalNetWeight = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.TotalNetWeight || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );

    const totalTotalVolume = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.TotalVolume || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );
    const totalfield1 = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.field1 || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );
    const totalfield2 = useMemo(
        () => tabledata.reduce((acc, curr) => parseFloat(acc) + parseFloat((curr.field2 || 0)), 0),
        [tabledata], // 确保在 data 变化时重新计算
    );


    const afdisAmount = useMemo(
        () => (totalfield2 / 100 * (crmdata?.MainDiscount || 0)).toFixed(3),
        [crmdata, totalfield2], // 确保在 data 变化时重新计算
    );

    //子表列配置
    const columns = useMemo(() => [
        { accessorKey: 'ForeignsacontractNo', header: '销售合同号', size: 130, minSize: 130, enableEditing: false },
        { accessorKey: 'OrderNo', header: '客户订单号', size: 120, minSize: 120, enableEditing: false },
        { accessorKey: 'Product.name', header: '产品编号', size: 130, minSize: 130, enableEditing: false },
        {
            accessorKey: 'AccountprtNo', header: '客户货号', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['AccountprtNo'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'internalcode', header: '内部货号', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['internalcode'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Englishname', header: '英文货名', size: 180, minSize: 180, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Englishname'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Specification', header: '英文规格', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Specification'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Brand', header: '品牌', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Brand'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        { accessorKey: 'PSNoticeNo', header: '生产通知单号', size: 130, minSize: 130, enableEditing: false, },
        { accessorKey: 'Quantity_so', header: '数量(总)', size: 130, minSize: 130, enableEditing: false, enableSorting: false, },
        { accessorKey: 'NumberofCartons_so', header: '箱数(总)', size: 130, minSize: 130, enableEditing: false, enableSorting: false, },
        { accessorKey: 'Quantity_unused', header: '可用数量', size: 130, minSize: 130, enableEditing: false, enableSorting: false, },
        {
            accessorKey: 'Qty', header: '数量(此次)', size: 130, minSize: 130, enableEditing: true, enableSorting: false,
            muiEditTextFieldProps: ({ cell, row, table }) => ({
                type: 'number',
                required: true, // 设置必填
                onBlur: (event) => {
                    // console.log(table); // 打印表格实例
                    // console.log(row); // 打印表格实例
                    // console.log(table.getAllColumns()); // 打印表格实例
                    const value = parseFloat(event.target.value);
                    const index = cell.row.index;
                    let qty = value || 0;
                    tabledata[index]['Qty'] = qty;

                    let boxnum = (qty / (tabledata[index]['QuantityperCarton'] || 1)).toFixed(2);

                    tabledata[index]['NumberofCartons'] = boxnum;
                    // Calculate other TotalVolumes based on the new quantity
                    tabledata[index]['TotalVolume'] = (tabledata[index]['Volume'] * boxnum).toFixed(4);
                    tabledata[index]['TotalGrossWeight'] = (tabledata[index]['Grossweight'] * boxnum).toFixed(4);
                    tabledata[index]['TotalNetWeight'] = (tabledata[index]['Netweight'] * boxnum).toFixed(4);
                    let total = (qty * (tabledata[index]['Unit_Price'] || 0));
                    tabledata[index]['Total'] = total.toFixed(3);

                    let discount_b = tabledata[index]['ItemDiscountRate'] || 0;
                    let discount = -(total / 100 * discount_b);
                    tabledata[index]['field1'] = discount.toFixed(2);
                    
                    tabledata[index]['field2'] = (total + parseFloat(discount)).toFixed(3);
                    const newTableData = [...tabledata];

                    setTabledata(newTableData); // Trigger re-render
                    // setTabledata({...tabledata,[row.id]:row.original }); // Trigger re-render
                },
            }),
        },
        {
            accessorKey: 'NumberofCartons', header: '箱数(此次)', size: 130, minSize: 130, enableEditing: true, enableSorting: false,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                required: true, // 设置必填
                onBlur: (event) => {
                    const value = parseFloat(event.target.value) || 0; // Ensure it's a number
                    const index = cell.row.index;
                    tabledata[index]['NumberofCartons'] = value;

                    // Calculate other TotalVolumes based on the new quantity
                    let qty = value * (tabledata[index]['QuantityperCarton'] || 1).toFixed(0);
                    tabledata[index]['Qty'] = qty;
                    tabledata[index]['TotalVolume'] = (tabledata[index]['Volume'] * value).toFixed(4);
                    tabledata[index]['TotalGrossWeight'] = (tabledata[index]['Grossweight'] * value).toFixed(4);
                    tabledata[index]['TotalNetWeight'] = (tabledata[index]['Netweight'] * value).toFixed(4);
                    let total = (qty * (tabledata[index]['Unit_Price'] || 0));

                    // tabledata[index]['Total'] = total.toFixed(3);
                    let discount_b = tabledata[index]['ItemDiscountRate'] || 0;
                    let discount = -(total / 100 * discount_b);
                    tabledata[index]['field1'] = discount.toFixed(2);
                    tabledata[index]['field2'] = (total + parseFloat(discount)).toFixed(3);
                   
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
            Footer: () => (
                <Stack>
                    <Box color="warning.main">{Math.round(totalNumberofCartons)}</Box>
                </Stack>
            ),
        },
        { accessorKey: 'QuantityperCarton', header: '每箱数量', size: 130, minSize: 130, enableEditing: false },
        {
            accessorKey: 'Unit_Price', header: '单价', size: 150, minSize: 150, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                required: true, // 设置必填
                onBlur: (event) => {
                    const value = parseFloat(event.target.value) || 0; // Ensure it's a number
                    const index = cell.row.index;
                    tabledata[index]['Unit_Price'] = value;
                    let qty = tabledata[index]['Qty'] || 0;
                    let total = (value * qty) || 0;
                    tabledata[index]['TotalAmount'] = (total).toFixed(3);

                    let discount_b = tabledata[index]['ItemDiscountRate'] || 0;
                    let discount = -(total / 100 * discount_b);
                    tabledata[index]['field1'] = discount.toFixed(2);


                    tabledata[index]['field2'] = (value * qty + parseFloat(discount)).toFixed(3);
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'TotalAmount', header: '折扣前总金额', size: 150, minSize: 150, enableEditing: false, Cell: ({ row, table }) => {
                const total = ((row.original.Qty || 0) * (row.original.Unit_Price || 0)).toFixed(2);
                table.options.data[table.getRowModel().rows.indexOf(row)].TotalAmount = total;
                return total; // Display with 2 decimal places
            },
            Footer: () => (
                <Stack>
                    <Box color="warning.main">{totalTotalAmount.toFixed(3)}</Box>
                </Stack>
            ),
        },
        {
            accessorKey: 'ItemDiscountRate', header: '单品折扣率', minSize: 140, size: 140, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = parseFloat(event.target.value || 0);
                    const index = cell.row.index;
                    let total = tabledata[index]['TotalAmount'] || 0;
                    let discount = -(total / 100 * value);
                    tabledata[index]['ItemDiscountRate'] = value;
                    tabledata[index]['field1'] = discount.toFixed(2);
                    tabledata[index]['field2'] = (parseFloat(total) + parseFloat(discount)).toFixed(2);
                    
                    setTabledata([...tabledata]);
                },
            }),
        },
        {
            accessorKey: 'field1', header: '单品折扣', size: 110, minSize: 110, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = parseFloat(event.target.value) || 0; // Ensure it's a number
                    const index = cell.row.index;

                    let totalAmount = tabledata[index]['TotalAmount'] || 0;
                    let itemDiscountRate = (-value / totalAmount * 100).toFixed(2);
                    tabledata[index]['ItemDiscountRate'] = itemDiscountRate;
                    tabledata[index]['field1'] = value;


                    tabledata[index]['field2'] = (parseFloat(totalAmount) + value).toFixed(3);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
            Footer: () => (
                <Stack>
                    <Box color="warning.main">{totalfield1.toFixed(3)}</Box>
                </Stack>
            ),
        },
        {
            accessorKey: 'field', header: '折扣原因', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || ""; // Ensure it's a number
                    const index = cell.row.index;
                    tabledata[index]['field'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'field2', header: '折扣后金额', size: 130, minSize: 130, enableEditing: false,

            Footer: () => (
                <Stack>
                    <Box color="warning.main">{`${totalfield2.toFixed(3)} - ${(parseFloat(afdisAmount || 0)).toFixed(3)} = ${(totalfield2 - afdisAmount).toFixed(3)}`}</Box>
                </Stack>
            ),
        },

        // {
        //     accessorKey: 'DomesticOrderAmount', header: '国内接单金额', size: 140, minSize: 140, enableEditing: true,
        //     muiEditTextFieldProps: ({ cell, row }) => ({
        //         type: 'number',
        //         onBlur: (event) => {
        //             const value = parseFloat(event.target.value) || 0; // Ensure it's a number
        //             const index = cell.row.index;
        //             tabledata[index]['DomesticOrderAmount'] = value;
        //             const newTableData = [...tabledata];
        //             setTabledata(newTableData);
        //         },
        //     }),
        //     Footer: ({ table }) => {
        //         const sum = table.getRowModel().rows.reduce((acc, row) => {
        //             const total = (row.original.DomesticOrderAmount || 0);
        //             return acc + total;
        //         }, 0);

        //         return (
        //             <Stack>
        //                 <Box color="warning.main">{sum.toFixed(4)}</Box>
        //             </Stack>
        //         );
        //     },
        // },
        {
            accessorKey: 'Unit', header: '单位', size: 110, minSize: 110, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Unit'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'HSCode', header: 'HS编码', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['HSCode'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        // { accessorKey: 'Nuofboxesinpg', header: '入数', size: 90, minSize: 90, enableEditing: false },
        // { accessorKey: 'Nuofboxeinnerbox', header: '内盒数', size: 90, minSize: 90, enableEditing: false },
        {
            accessorKey: 'Packaginunit', header: '包装单位', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Packaginunit'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Lengthofouterbox', header: '长(CM)', size: 110, minSize: 110, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['Lengthofouterbox'] = value;

                    let width = tabledata[index]['Cartonwidth'];
                    let height = tabledata[index]['Heightofouterbox'];
                    let volume = ((width * height * value) / 1000000);
                    tabledata[index]['Volume'] = volume.toFixed(4);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Cartonwidth', header: '宽(CM)', size: 110, minSize: 110, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['Cartonwidth'] = value;

                    let Lengt = tabledata[index]['Lengthofouterbox'];
                    let height = tabledata[index]['Heightofouterbox'];
                    let volume = ((Lengt * height * value) / 1000000);
                    tabledata[index]['Volume'] = volume.toFixed(4);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Heightofouterbox', header: '高(CM)', size: 110, minSize: 110, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['Heightofouterbox'] = value;


                    let Lengt = tabledata[index]['Lengthofouterbox'];
                    let width = tabledata[index]['Cartonwidth'];
                    let volume = ((Lengt * width * value) / 1000000);
                    tabledata[index]['Volume'] = volume.toFixed(4);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Volume', header: '体积（CBM）', size: 140, minSize: 140, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['Volume'] = value;

                    let boxnum = tabledata[index]['NumberofCartons'];
                    tabledata[index]['TotalVolume'] = (boxnum * value).toFixed(4);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Netweight', header: '外箱净重(KG)', size: 140, minSize: 140, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['Netweight'] = value;

                    let boxnum = tabledata[index]['NumberofCartons'];
                    tabledata[index]['TotalNetWeight'] = (boxnum * value).toFixed(4);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Grossweight', header: '外箱毛重(KG)', size: 140, minSize: 140, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['Grossweight'] = value;

                    let boxnum = tabledata[index]['NumberofCartons'];
                    tabledata[index]['TotalGrossWeight'] = (boxnum * value).toFixed(4);

                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'TotalGrossWeight', header: '总毛重', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['TotalGrossWeight'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
            Footer: () => (
                <Stack>
                    <Box color="warning.main">{totalTotalGrossWeight.toFixed(4)}</Box>
                </Stack>
            ),
        },
        {
            accessorKey: 'TotalNetWeight', header: '总净重', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['TotalNetWeight'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
            Footer: () => (
                <Stack>
                    <Box color="warning.main">{totalTotalNetWeight.toFixed(4)}</Box>
                </Stack>
            ),
        },
        {
            accessorKey: 'TotalVolume', header: '总体积', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'number',
                onBlur: (event) => {
                    const value = event.target.value || 0;
                    const index = cell.row.index;
                    tabledata[index]['TotalVolume'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
            Footer: () => (
                <Stack>
                    <Box color="warning.main">{totalTotalVolume.toFixed(4)}</Box>
                </Stack>
            ),
        },
        {
            accessorKey: 'Remarks', header: '备注', size: 180, minSize: 180, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Remarks'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Productname', header: '中文品名', size: 180, minSize: 180, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Productname'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Chinesespecification', header: '中文规格', size: 130, minSize: 130, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Chinesespecification'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'ChinesePackagedesp', header: '中文包装描述', size: 250, minSize: 250, enableEditing: canEdit,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['ChinesePackagedesp'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        { accessorKey: 'Category', header: '类别', size: 130, minSize: 130, enableEditing: canEdit },
        {
            accessorKey: 'ClearanceCurrency', header: '清关币种', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['ClearanceCurrency'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'BoxNum', header: '箱号', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['BoxNum'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'ShippingMark', header: '唛头', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['ShippingMark'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Batch', header: '批次', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Batch'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'BatchNumber', header: '批次号', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['BatchNumber'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        {
            accessorKey: 'Tastingperiod', header: '赏味期', size: 130, minSize: 130, enableEditing: true,
            muiEditTextFieldProps: ({ cell, row }) => ({
                type: 'text',
                onBlur: (event) => {
                    const value = event.target.value || "";
                    const index = cell.row.index;
                    tabledata[index]['Tastingperiod'] = value;
                    const newTableData = [...tabledata];
                    setTabledata(newTableData);
                },
            }),
        },
        // { accessorKey: 'Chineseingredients', header: '中文成份', size: 130, minSize: 130, enableEditing: false },
    ], [tabledata, canEdit, totalTotalGrossWeight, totalNumberofCartons, totalTotalAmount, totalTotalNetWeight, totalTotalVolume, totalfield1, totalfield2, afdisAmount]);


    //子表配置
    const table = useMaterialReactTable({
        columns: columns,
        data: tabledata ?? [],
        enableRowOrdering: true, // 行排序
        enablePagination: false, // 禁用分页
        enableColumnOrdering: true, // 列排序
        enableColumnFilters: false, // 列过滤
        enableStickyHeader: true, // 启用粘性表头
        enableGlobalFilter: false, // 禁用全局过滤
        localization: LocalizationZH, // 设置中文
        enableColumnResizing: true, // 启用列调整大小
        editDisplayMode: 'cell', // 编辑模式为单元格
        enableEditing: true,  // 启用编辑
        enableDensityToggle: false, // 禁用密度切换
        // enableRowOrdering: true, // 启用行排序
        enableSorting: false, // 禁用排序
        enableFullScreenToggle: false, // 禁用全屏按钮
        enableColumnPinning: true, // 启用列固定
        // getRowId: (row) => row.id, // 设置每行的唯一标识
        positionToolbarAlertBanner: 'bottom',   // 将工具栏警告横幅的位置设置为底部
        layoutMode: 'grid-no-grow', // 设置布局模式为网格，并且不增长
        initialState: {
            density: 'compact',
            columnPinning: { left: ['mrt-row-drag', 'mrt-row-actions', 'mrt-row-numbers', 'Product.name', 'AccountprtNo', 'internalcode'] },  //'mrt-row-actions',
        },
        muiTableContainerProps: {
            sx: {
                minHeight: '80vh', // 这里设置表格最大高度
                maxHeight: '80vh', // 这里设置表格最大高度
                overflowY: 'auto',  // 启用竖向滚动条
                overflowX: 'auto',  // 启用横向滚动条，横向滚动条默认显示
            },
        },
        muiTableHeadCellProps: ({ column }) => ({
            sx: {
                backgroundColor: 'rgb(244, 247, 255)',
                ...(column.columnDef.required && {
                    '&::after': {
                        content: '"*"',
                        color: 'red',
                        marginLeft: '30px',
                        position: 'absolute',
                    }
                })
            }
        }),
        muiRowDragHandleProps: ({ table }) => ({
            onDragEnd: () => {
                const { draggingRow, hoveredRow } = table.getState();
                if (hoveredRow && draggingRow) {
                    tabledata.splice(
                        hoveredRow.index,
                        0,
                        tabledata.splice(draggingRow.index, 1)[0],
                    );
                    setTabledata([...tabledata]);
                }
            },
        }),
        enableRowNumbers: true,
        rowNumberDisplayMode: 'static', // 显示原始行号
        renderTopToolbarCustomActions: ({ table }) => (
            <Box sx={{ display: 'flex', gap: '1rem', p: '4px' }}>
                <Button
                    // color="primary"
                    onClick={() => {
                        sodetailGet(); // 获取合同明细数据
                        setOpenDrawer_sodetail(true); // 打开合同明细抽屉
                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: 'rgb(74, 146, 243)',
                    }}
                    disabled={isloacked}
                >
                    从合同选取
                </Button>
                <Button
                    onClick={() => {
                        productsGet(); // 获取商品库数据
                        setOpenDrawer_product(true); // 打开商品库抽屉
                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: 'rgb(74, 146, 243)',
                    }}
                >
                    从商品库选取
                </Button>
                <Button
                    // color="primary"
                    onClick={async () => {
                        await handleSave(); // 保存数据
                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: 'rgb(74, 146, 243)',
                    }}
                    disabled={isloacked}
                >
                    保存
                </Button>
                <Button
                    onClick={() => {
                        window.ZOHO.CRM.UI.Popup.close();
                    }}
                    variant="outlined"
                >
                    取消
                </Button>
            </Box>
        ),
        enableRowActions: true,
        displayColumnDefOptions: {
            'mrt-row-actions': {
                header: '操作',  // 设置表头为“操作”
                size: 4,  // 设置列宽
            },
            'mrt-row-drag': {
                header: '',  // 设置表头为“操作”
                size: 4,  // 设置列宽
            },
        },
        renderRowActions: ({ row, table }) => (
            <Box>
                <Clear onClick={() => {
                    if (!isloacked) {
                        // setTabledata((prevRecords) => prevRecords.filter((record) => record.id !== row.id)); // 从记录中移除当前行
                        tabledata.splice(row.index, 1); //assuming simple data table
                        setTabledata([...tabledata]);
                    }
                }}>
                </Clear>
                <CopyAll onClick={() => {
                    if (!isloacked) {
                        // 复制当前行数据
                        const newRow = { ...row.original }; // 生成新的 ID
                        delete newRow.id;
                        // // 插入到当前数据下面
                        // const index = table.getRowModel().rows.findIndex(r => r.id === row.id);
                        // const newData = [
                        //     ...tabledata.slice(0, index + 1),
                        //     newRow,
                        //     ...tabledata.slice(index + 1)
                        // ];
                        const newData = [...tabledata, newRow]; // 插入到当前数据下面
                        setTabledata(newData); // 更新数据
                    }
                }}>
                </CopyAll>
            </Box>
        ),
        enableStickyFooter: true, // 启用粘性页脚
        // muiEditTextTotalVolumeProps: ({ cell }) => ({
        //   onBlur: (event) => {
        //     handleSaveCell(cell, event.target.value); // 在失焦时保存
        //   },
        // }),
        renderToolbarInternalActions: ({ table }) => (
            <Box>
                <IconButton
                    onClick={() => {
                        exportToExcel(tabledata, columns);
                    }}
                >
                    <GetAppOutlined />
                </IconButton>
                <ShowHideColumnsButton table={table} />
            </Box>
        ),
        enableKeyboardShortcuts: false,
        defaultColumn: {
            muiTableBodyCellProps: ({ row, cell }) => ({
                onKeyDown: (event) => { // 添加键盘事件监听
                    if (event.key === 'Enter') {
                        const nextrownum = row.index + 1; // 获取下一行的索引
                        const currentIndex = cell.column.getIndex();
                        if (nextrownum < tabledata.length) {
                            const nextRow = document.querySelector(
                                `tr[data-index="${nextrownum}"]`
                            );

                            // 在该行中找到对应列的单元格
                            if (nextRow) {
                                const nextCell = nextRow.querySelector(
                                    `td[data-index="${currentIndex}"]`
                                );
                                if (nextCell) {
                                    nextCell.focus();
                                    const dblClickEvent = new MouseEvent('dblclick', {
                                        bubbles: true,
                                        cancelable: true,
                                        view: window
                                    });
                                    nextCell.dispatchEvent(dblClickEvent);
                                }
                            }
                        }
                    }
                },
                tabIndex: 0,
            })
        },
    });


    //产品选择配置
    const table_product = useMaterialReactTable({
        columns: columns_product,
        data: productdata || [],
        enableRowSelection: true,  // 启用行选择
        enableColumnResizing: true, // 启用列调整大小
        enableStickyHeader: true, // 启用粘性表头
        enableDensityToggle: false, // 禁用密度切换
        enableGlobalFilter: false, // 禁用全局过滤
        state: {
            // rowSelection_product, // 设置行选择状态
            showLoadingOverlay: loading, // 设置加载状态
        },
        enableFullScreenToggle: false, // 禁用全屏按钮
        enableColumnPinning: true, // 启用列固定
        localization: LocalizationZH, // 设置中文
        // getRowId: (row) => row.id, // 设置每行的唯一标识
        positionToolbarAlertBanner: 'bottom',   // 将工具栏警告横幅的位置设置为底部
        initialState: {
            density: 'compact',
            columnPinning: { left: ['mrt-row-select', 'mrt-row-numbers', 'Product_Name', 'AccountprtNo'] },
            pagination: { pageSize: 20, },
            showColumnFilters: true, // 显示列过滤器
            columnVisibility: { Lengthofouterbox: false, Cartonwidth: false, Heightofouterbox: false, Volume: false, Netweightofinnebox: false, Nuofboxesinpg: false, Nuofboxeinnerbox: false, Packagingquantityperpg: false, Netweight: false, Grossweight: false, Packaginunit: false, Declarationelements: false, HSCode_text: false, }
        },
        muiTableContainerProps: {
            sx: {
                minHeight: '75vh', // 这里设置表格最大高度
                maxHeight: '75vh', // 这里设置表格最大高度
                overflowY: 'auto',  // 启用竖向滚动条
                overflowX: 'auto',  // 启用横向滚动条，横向滚动条默认显示
            },
        },
        // onRowSelectionChange: setRowSelection_product, // 当行选择发生变化时，更新行选择状态
        renderTopToolbarCustomActions: ({ table }) => (
            <Box sx={{ display: 'flex', gap: '1rem', p: '4px' }}>
                {canEdit && (
                    <TextField
                        placeholder="请输入商品编码或者客户货号的关键字进行搜索" // 设置搜索框的占位符
                        variant="outlined"  // 使用 Material-UI 的 outlined 风格
                        size="small"        // 设置搜索框的尺寸
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => {
                                            productsGet(searchValue); // 调用搜索函数
                                        }}
                                    >
                                        <Search />
                                    </IconButton>
                                </InputAdornment>
                            ),
                            onChange: (e) => {
                                setSearchValue(e.target.value);
                            }, // 添加 onChange 事件处理函数
                            sx: {
                                width: '100%', // 设置输入框宽度为100%
                                borderRadius: '6px', // 设置圆角
                                backgroundColor: '#f5f7f8', // 背景颜色
                            },
                        }}
                        sx={{ width: '400px' }} // 设置搜索框宽度
                    />
                )}
                <Button
                    onClick={() => {
                        // 从选中的行中提取产品信息
                        const selecteds = table.getSelectedRowModel().rows;

                        if (selecteds.length === 0) {
                            openSnackbar(`请选择至少一个产品！`, 'info');
                            return;
                        }
                        const selectedProducts = selecteds.map(item => item.original);

                        let resultarray = [];
                        selectedProducts.forEach((product) => {
                            console.log(product);
                            const findprid = tabledata.find((item) => item?.Product?.id === product?.id);
                            if (!findprid) {
                                resultarray.push({

                                    Product: { id: product.id, name: product.Product_Name }, // 产品名称
                                    Product_Id: product.id, // 产品ID
                                    AccountprtNo: product.AccountprtNo,
                                    // ForeignsacontractNo: product?.Subject, // 外贸合同号
                                    // OrderNo: product?.AccountOrderNo, // 订单号
                                    // PSNoticeNo: product?.W,  //生产通知单号
                                    ShippingMark: product?.ShippingMark, // 货物唛头
                                    // WItemId: product?.WItemId,      //生产发货通知明细id
                                    internalcode: product.internalcode, //内部编码
                                    Englishname: product?.Englishname,
                                    Specification: product?.Specification,
                                    List_Price: product?.List_Price, // 价格
                                    // Discount: product?.Discount,  //折扣
                                    // SNQty: product?.SNQty,
                                    // Quantity_so: product?.Quantity,
                                    // NumberofCartons_so: product?.NumberofCartons,  //箱数
                                    // Quantity_unused: product?.Quantity - (product?.SNQty || 0), //可用数量 = 总数量 - 已分配数量
                                    // QuantityperCarton: product?.Packagingquantityperpg,  // 每箱数量
                                    // field1: product?.Discount,     
                                    // field2: product?.Net_Total,       // 折扣后金额
                                    // field:product?.,           //
                                    // Qty: product?.Quantity - (product?.SNQty || 0),
                                    // NumberofCartons: boxnum,  //箱数
                                    Packagingquantityperpg: product?.Packagingquantityperpg,   // 每箱数量
                                    // Unit_Price: product?.Unit_Price,
                                    // TotalNetWeight: (boxnum * product?.Netweight).toFixed(4),  //总净重
                                    // TotalGrossWeight: (boxnum * product?.Grossweight).toFixed(4), // 总毛重
                                    // TotalVolume: (boxnum * product?.Volume).toFixed(4), // 总体积
                                    Unit: product?.Unit,
                                    Volume: product?.Volume,
                                    Brand: product?.Brand,
                                    Nuofboxesinpg: product?.Nuofboxesinpg,
                                    Nuofboxeinnerbox: product?.Nuofboxeinnerbox,
                                    Lengthofouterbox: product?.Lengthofouterbox,
                                    Cartonwidth: product?.Cartonwidth,
                                    Heightofouterbox: product?.Heightofouterbox,
                                    Netweight: product?.Netweight,
                                    Grossweight: product?.Grossweight,
                                    Productname: product.Productname, // 产品名称
                                    ChinesePackagedesp: product?.ChinesePackagedesp,
                                    Chinesespecification: product?.Chinesespecification,
                                    // Chineseingredients: product?.Chineseingredients,
                                    Chptrequirements_material: product?.Chptrequirements_material,
                                    Category: product?.Category,
                                    Packaginunit: product?.Packaginunit,
                                    HSCode: product?.HSCode,
                                    // id1: product?.id,
                                    // SalesOrderId: product?.pid,
                                    // id: `${product?.id}new`,

                                });
                            }
                        })
                        // 将选中的产品添加到主表中
                        setTabledata((prevData) => [...prevData, ...resultarray]);
                        // // 可选：清空行选择状态
                        // setRowSelection_product({});


                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: 'rgb(74, 146, 243)', // 背景颜色设置为白色
                    }}
                >
                    添加产品
                </Button>
                <Button
                    onClick={() => {
                        setOpenDrawer_product(false); // 关闭抽屉组件
                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: '#ffffff', // 背景颜色设置为白色
                        color: '#000000', // 字体颜色设置为黑色
                    }}
                >
                    我已经选取完成了
                </Button>
            </Box>
        ),
    });

    //选择合同明细配置
    const table_sodetail = useMaterialReactTable({
        columns: columns_sodetail,
        data: sodetaildata || [],
        enableColumnResizing: true, // 启用列调整大小
        enableStickyHeader: true, // 启用粘性表头
        enableRowNumbers: true,
        enablePagination: false, // 禁用分页
        enableDensityToggle: false, // 禁用密度切换
        enableGlobalFilter: false, // 禁用全局过滤
        localization: LocalizationZH, // 设置中文
        state: {
            rowSelection, // 设置行选择状态
            showLoadingOverlay: loading, // 设置加载状态
        },
        enableFullScreenToggle: false, // 禁用全屏按钮
        enableColumnPinning: true, // 启用列固定
        getRowId: (row) => row.id, // 设置每行的唯一标识
        // rowPinningDisplayMode: 'select-sticky', // 设置行固定显示模式为选择固定
        positionToolbarAlertBanner: 'bottom',   // 将工具栏警告横幅的位置设置为底部
        initialState: {
            density: 'compact',
            columnPinning: { left: ['mrt-row-select', 'mrt-row-numbers', 'Subject', 'AccountOrderNo', 'pname', 'AccountprtNo'] },
            // pagination: { pageSize: 20, },
            showColumnFilters: true, // 显示列过滤器
            columnVisibility: { Lengthofouterbox: false, Cartonwidth: false, Heightofouterbox: false, Volume: false, Netweightofinnebox: false, Nuofboxesinpg: false, Nuofboxeinnerbox: false, Packagingquantityperpg: false, Netweight: false, Grossweight: false, Packaginunit: false, Declarationelements: false, HSCode_text: false, }
        },
        muiTableContainerProps: {
            sx: {
                minHeight: '80vh', // 这里设置表格最大高度
                maxHeight: '80vh', // 这里设置表格最大高度
                overflowY: 'auto',  // 启用竖向滚动条
                overflowX: 'auto',  // 启用横向滚动条，横向滚动条默认显示
            },
        },
        // enableRowNumbers: true,
        // rowNumberDisplayMode: 'static', // 显示原始行号
        // enableRowSelection: true,   // 启用行选择
        enableRowSelection: (row) => {
            const categorys = ['费用类', '赠品类'];
            const snQtyLessThanQuantity = (row.original.SNQty || 0) < (row.original.Quantity || 0);
            const notSelected = !row.original.selected;
            const hasWItemId = row.original.WItemId;
            const isCategoryFee = categorys.includes(row.original.Category);


            return ((snQtyLessThanQuantity && hasWItemId) || isCategoryFee) && notSelected;
        }, // 启用行选择
        onRowSelectionChange: setRowSelection, // 当行选择发生变化时，更新行选择状态
        renderTopToolbarCustomActions: ({ table }) => (
            <Box sx={{ display: 'flex', gap: '1rem', p: '4px' }}>
                <Button
                    onClick={() => {
                        // console.log(rowSelection);
                        // 从选中的行中提取产品信息
                        const selectedProductIds = Object.keys(rowSelection).filter((key) => rowSelection[key]); // 获取选中的产品 ID
                        const selectedProducts = selectedProductIds.map((id) => {
                            const selectedRow = sodetaildata.find((product) => product.id === id); // 获取选中的产品
                            return selectedRow; // 返回选中的产品信息
                        }).filter(Boolean); // 过滤掉未找到的产品

                        // console.log('selectedProducts', selectedProducts);

                        let resultarray = [];
                        selectedProducts.forEach((product) => {
                            const findedProduct = tabledata.find((item) => item.id1 === product.id);
                            if (!findedProduct) {
                                let boxnum = (product?.Quantity - (product?.SNQty || 0)) / (product?.Packagingquantityperpg || 1); // 计算箱数
                                resultarray.push({
                                    Product: { id: product?.pdid, name: product?.pname },// 产品信息
                                    AccountprtNo: product?.AccountprtNo,
                                    ForeignsacontractNo: product?.Subject,
                                    OrderNo: product?.AccountOrderNo,
                                    PSNoticeNo: product?.W,  //生产通知单号
                                    ShippingMark: product?.ShippingMark,
                                    WItemId: product?.WItemId,                              //生产发货通知明细id
                                    internalcode: product?.internalcode,
                                    Englishname: product?.Englishname,
                                    Specification: product?.Specification,
                                    Unit_Price: product?.List_Price,
                                    ItemDiscountRate: product?.ItemDiscountRate, //折扣率
                                    field1: product?.Discount,  //折扣  
                                    field2: product?.Net_Total,       // 折扣后金额
                                    SNQty: product?.SNQty,
                                    Quantity_so: product?.Quantity,
                                    NumberofCartons_so: product?.NumberofCartons,  //箱数
                                    Quantity_unused: product?.Quantity - (product?.SNQty || 0), //可用数量 = 总数量 - 已分配数量
                                    QuantityperCarton: product?.Packagingquantityperpg,  // 每箱数量
                                    // field1: product?.field1,           
                                    // field:product?.,           //
                                    Qty: product?.Quantity - (product?.SNQty || 0),
                                    NumberofCartons: boxnum,  //箱数
                                    Packagingquantityperpg: product?.Packagingquantityperpg,
                                    // Unit_Price: product?.Unit_Price,
                                    TotalNetWeight: (boxnum * product?.Netweight).toFixed(4),  //总净重
                                    TotalGrossWeight: (boxnum * product?.Grossweight).toFixed(4), // 总毛重
                                    TotalVolume: (boxnum * product?.Volume).toFixed(4), // 总体积
                                    Unit: product?.Unit,
                                    Volume: product?.Volume,
                                    Brand: product?.Brand,
                                    Nuofboxesinpg: product?.Nuofboxesinpg,
                                    Nuofboxeinnerbox: product?.Nuofboxeinnerbox,
                                    Lengthofouterbox: product?.Lengthofouterbox,
                                    Cartonwidth: product?.Cartonwidth,
                                    Heightofouterbox: product?.Heightofouterbox,
                                    Netweight: product?.Netweight,
                                    Grossweight: product?.Grossweight,
                                    Productname: product?.Productname,
                                    ChinesePackagedesp: product?.ChinesePackagedesp,
                                    Chinesespecification: product?.Chinesespecification,
                                    // Chineseingredients: product?.Chineseingredients,
                                    Chptrequirements_material: product?.Chptrequirements_material,
                                    Category: product?.Category,
                                    Packaginunit: product?.Packaginunit,
                                    HSCode: product?.HSCode,
                                    id1: product?.id,
                                    SalesOrderId: product?.pid,
                                    id: `${product?.id}new`,

                                });
                            }
                        })


                        // console.log(resultarray);

                        // 将选中的产品添加到主表中
                        setTabledata((prevData) => [...prevData, ...resultarray]);

                        // 可选：清空行选择状态
                        setRowSelection({});
                        setOpenDrawer_sodetail(false); // 关闭抽屉组件

                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: 'rgb(74, 146, 243)', // 背景颜色设置为白色
                    }}
                >
                    确定
                </Button>
                <Button
                    onClick={() => {
                        setOpenDrawer_sodetail(false); // 关闭抽屉组件
                    }}
                    variant="contained"
                    sx={{
                        backgroundColor: '#ffffff', // 背景颜色设置为白色
                        color: '#000000', // 字体颜色设置为黑色
                    }}
                >
                    关闭窗口
                </Button>
            </Box>
        ),
    });

    return (
        <>
            <div>
                {loading ? (
                    <Box
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '100vh' // 使加载图标在页面中央
                        }}
                    >
                        <CircularProgress />
                    </Box>
                ) : (
                    <>
                        <Grid direction="row"
                            container spacing={2}
                            sx={{
                                justifyContent: "flex-start",
                                alignItems: "flex-start",
                            }}>
                            <Grid item="true" xs={4} md={1}>
                                <TextField
                                    disabled
                                    label="客户"
                                    value={crmdata?.Account_Name?.name || ''}
                                    size="small"
                                />
                            </Grid>
                            <Grid item="true" xs={4} md={1}>
                                <TextField
                                    disabled
                                    label="香港留点比例"
                                    value={crmdata?.RetentionRate || ''}
                                    size="small"
                                />
                            </Grid>
                            <Grid item="true" xs={4} md={1}>
                                <TextField
                                    disabled
                                    label="合同总折扣%"
                                    value={crmdata?.MainDiscount || ''}
                                    onChange={(e) => setCrmdata((prevData) => ({ ...prevData, MainDiscount: e.target.value }))}
                                    size="small"
                                />
                            </Grid>
                            <Grid item="true" xs={4} md={1}>
                                <TextField
                                    disabled
                                    label="合同总折扣金额"
                                    value={afdisAmount || ''} // 这里需要计算
                                    size="small"
                                />
                            </Grid>
                        </Grid>
                        <MaterialReactTable
                            table={table} />
                    </>
                )}
            </div>
            {/* 合同明细抽屉组件 */}
            <Drawer
                anchor="left"
                open={openDrawer_sodetail}
                sx={{ width: '75vw', zIndex: 1300 }}
            >
                <Box sx={{ width: '75vw', padding: '2px' }}>
                    <MaterialReactTable
                        table={table_sodetail}
                    />
                </Box>
            </Drawer >
            {/* 商品库抽屉组件 */}
            <Drawer
                anchor="left"
                open={openDrawer_product}
                sx={{ width: '75vw', zIndex: 1300 }}
            >
                <Box sx={{ width: '75vw', padding: '2px' }}>
                    <MaterialReactTable
                        table={table_product}
                    />
                </Box>
            </Drawer >
        </>
    );
}



export default App;