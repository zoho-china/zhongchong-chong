/*global $Client*/
import React, { useState, useEffect, useRef } from "react";
import { Spin, message, Button } from "antd";
import { ListTable, Menu, ListColumn } from "@visactor/react-vtable";
import { ExportOutlined } from "@ant-design/icons";
import { downloadExcel, exportVTableToExcel } from "@visactor/vtable-export";
import * as VTable from "@visactor/vtable";
import * as VTable_editors from "@visactor/vtable-editors";
import { round } from 'mathjs'
import { customPrompt } from '../assembly/customprompt'



const input_editor = new VTable_editors.InputEditor();
VTable.register.editor("input-editor", input_editor);



//报关单明细批量操作
const App = () => {
  const [loading, setLoading] = useState(false);
  const [records, setRecords] = useState(null);
  const tableInstance = useRef(null);
  const [messageApi, contextHolder] = message.useMessage();



  useEffect(() => {
    const resizeAndFetchData = async () => {
      setLoading(true);
      await new Promise(resolve => setTimeout(resolve, 1000));
      fetchData();
    };

    resizeAndFetchData();
  }, []);

  const fetchData = async () => {
    try {
      setLoading(true);
      let pagedata = window.wdata;
      // console.log("pagedata", pagedata);
      const DeclarationSubfrom = pagedata?.data?.DeclarationSubfrom;
      // console.log("DeclarationSubfrom", DeclarationSubfrom);
      DeclarationSubfrom.forEach((item) => {
        item.PdName = item?.Product?.name;
      });

      console.log("DeclarationSubfrom", DeclarationSubfrom);

      if (DeclarationSubfrom) {
        setRecords(DeclarationSubfrom);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    } finally {
      setLoading(false);
    }
  };


  const onDropdownMenuClick = async (args) => {
    if (args.text === "按照此条件过滤") {
      console.log("按照此条件过滤");

      var titlename = tableInstance.current.getHeaderField(args.col, args.row);
      var cellValue = tableInstance.current.getCellValue(args.col, args.row);
      if (!cellValue) {
        messageApi.info("所选单元格值为空!");
        return;
      }
      tableInstance.current.updateFilterRules([
        {
          filterKey: titlename,
          filteredValues: cellValue,
        },
      ]);
    } else if (args.text === "清除过滤条件") {
      console.log("清除过滤条件");
      tableInstance.current.updateFilterRules();
    } else if (args.text === "批量修改") {
      let selectCells = tableInstance.current.getSelectedCellInfos();

      console.log(selectCells);
      if (!checkColumns(selectCells)) {
        messageApi.info("只能批量修改同一列的数据!");
        return;
      }
      // 获取列的字段名
      const colField = tableInstance.current.getHeaderField(selectCells[0][0].col, selectCells[0][0].row);
      
      // 从 option.columns 中找到对应的列配置
      const columnConfig = option.columns.find(column => column.field === colField);
      
      // 检查该列是否有 editor 属性，并且等于 "input-editor"
      if (!columnConfig || columnConfig.editor !== "input-editor") {
        messageApi.info("所选字段不可编辑!");
        return;
      }
      // const userInput = prompt("请输入值:", "");
      const userInput = await customPrompt("请输入值:");

      console.log("userInput", userInput);
      console.log("selectCells", selectCells);
      if (userInput !== null) {
        selectCells.forEach((item) => {
          tableInstance.current.changeCellValue(item[0].col, item[0].row, userInput);
        });
      }
    }
  };


  const ready = (instance, isInitial) => {
    if (isInitial) {
      tableInstance.current = instance;
    }
  };
  const checkColumns = (array) => {
    // 获取所有子数组的 col 值
    const cols = array.map((subArray) =>
      subArray.length === 1 ? subArray[0].col : null
    );

    // 过滤掉 null 值
    const filteredCols = cols.filter((col) => col !== null);

    // 检查所有 col 值是否相同
    const allEqual = filteredCols.every((col) => col === filteredCols[0]);

    return allEqual && filteredCols.length === array.length;
  };
  const option = {
    columns: [
      {
        field: "LinkingModule1_Serial_Number",
        title: "序号",
        width: "auto",
        aggregation: {
          aggregationType: VTable.TYPES.AggregationType.NONE,
          formatFun(value) {
            return "合计:";
          },
        },
      },
      {
        field: "DeclarationcontractNo",
        title: "报关合同单号",
        width: "auto",
        editor: "input-editor",
      },
      {
        field: "PdName",
        title: "商品编码",
        width: "auto",
        disableSelect: true,
      },
      {
        field: "HSName",
        title: "海关编码",
        width: "auto",
        disableSelect: false,
      },
      { field: "Productname", title: "中文品名", width: "auto" },
      { field: "Englishname", title: "英文品名", width: "auto" },
      {
        field: "CustomsdeclarationName",
        title: "报关品名",
        width: "auto",
        editor: "input-editor",
      },
      {
        field: "Qty",
        title: "数量",
        width: "auto",
        aggregation: {
          aggregationType: VTable.TYPES.AggregationType.SUM,
          formatFun(value) {
            return round(value, 2);
          },
        },
      },
      { field: "Unit", title: "单位", width: "auto" },
      { field: "UnitPrice", title: "单价含税", width: "auto" },
      { field: "UnitNotPrice", title: "单价不含税", width: "auto" },
      { field: "DeclarationAmount", title: "报关金额含税", width: "auto" },
      { field: "CustomsDeclaration", title: "报关不含税", width: "auto" },
      { field: "DiscountMoney", title: "折扣金额", width: "auto" ,editor: "input-editor",},
      {
        field: "TotalGrossWeight",
        title: "总毛重",
        width: "auto",
        aggregation: {
          aggregationType: VTable.TYPES.AggregationType.SUM,
          formatFun(value) {
            return round(value, 2);
          },
        },
      },
      {
        field: "TotalNetWeight",
        title: "总净重",
        width: "auto",
        aggregation: {
          aggregationType: VTable.TYPES.AggregationType.SUM,
          formatFun(value) {
            return round(value, 2);
          },
        },
      },
      {
        field: "TotalVolume",
        title: "总体积",
        width: "auto",
        aggregation: {
          aggregationType: VTable.TYPES.AggregationType.SUM,
          formatFun(value) {
            return round(value, 2);
          },
        },
      },
      {
        field: "QuantityperCarton",
        title: "每箱装量",
        width: "auto",
        aggregation: {
          aggregationType: VTable.TYPES.AggregationType.SUM,
          formatFun(value) {
            return round(value, 2);
          },
        },
      },
      { field: "Packaginunit", title: "包装单位", width: "auto" },
      { field: "Grossweight", title: "外箱毛重(KG)", width: "auto" },
      { field: "Netweight", title: "单净重(G)", width: "auto" },
      { field: "Volume", title: "体积", width: "auto" },
      { field: "Declarationelement", title: "申报要素", width: "auto" },
      { field: "DomesticSource", title: "境内货源地", width: "auto" },
      { field: "PSNoticeNo", title: "生产通知单号", width: "auto" },
      { field: "ForeignsacontractNo", title: "外销单号", width: "auto" },
    ],
  };

  const handtooltipfeilds = [
    "Qty",
    "UnitPrice",
    "UnitNotPrice",
    "DeclarationAmount",
    "TotalGrossWeight",
    "TotalNetWeight",
    "TotalVolume",
    "QuantityperCarton",
  ];

  const success = async () => {
    setLoading(true);
    try{
      tableInstance.current.updateFilterRules();

      console.log("tableInstance.current", tableInstance.current.records);
      $Client.close({data_updata: tableInstance.current.records});
      
    }catch (error) {
      
    }finally{
      setLoading(false);
    }


  };

  const handleExport = async () => {
    if (tableInstance.current) {
      downloadExcel(await exportVTableToExcel(tableInstance.current), "export");
    } else {
      messageApi.info("没有数据可以导出！");
    }
  };
  const cancel = async () => {
    $Client.close();

  };
  const handTooltip = (args) => {
    const { col, row } = args;
    let selectCells = tableInstance.current.getSelectedCellInfos();
    var titlename = tableInstance.current.getHeaderField(col, row);

    if (
      checkColumns(selectCells) &&
      selectCells.length > 1 &&
      handtooltipfeilds.includes(titlename)
    ) {
      const valueSum = selectCells.reduce((sum, item) => {
        const value = parseFloat(item[0].value) || 0; // Ensure value is a number
        return sum + value;
      }, 0);

      const rect = tableInstance.current.getVisibleCellRangeRelativeRect({
        col,
        row,
      });
      tableInstance.current.showTooltip(col, row, {
        content: "小计：" + round(valueSum),
        referencePosition: { rect, placement: VTable.TYPES.Placement.bottom }, //TODO
        className: "defineTooltip",
        style: {
          bgColor: "white",
          color: "black",
          arrowMark: true,
        },
      });
    }
  };

  return (
    <div>
      {contextHolder}
      <Spin spinning={loading}>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            gap: "10px",
            position: "fixed",
            top: 10,
            right: 10,
            zIndex: 9999,
          }}
        >
          <Button onClick={cancel}>
            取消
          </Button>
          <Button type="primary" onClick={success}>
            确定
          </Button>
          <Button
            type="primary"
            shape="circle"
            icon={<ExportOutlined />}
            onClick={handleExport}
          />
        </div>
        <div style={{ paddingTop: "50px" }}>
          <ListTable
            records={records}
            height={"500px"}
            widthMode="standard"
            theme={VTable.themes.ARCO}
            dragHeaderMode="column"
            onReady={ready}
            onMouseEnterCell={handTooltip}
            frozenColCount={2}
            bottomFrozenRowCount={1}
            onDropdownMenuClick={onDropdownMenuClick}
          >
            {option.columns.map((column) => (
              <ListColumn
                key={column.field}
                field={column.field}
                title={column.title}
                width={column.width}
                editor={column.editor}
                disableSelect={column.disableSelect}
                aggregation={column.aggregation}
              />
            ))}
            <Menu
              renderMode={"html"}
              contextMenuItems={["按照此条件过滤", "清除过滤条件", "批量修改"]}
            />
          </ListTable>
        </div>
      </Spin>
    </div>
  );
};

export default App;
