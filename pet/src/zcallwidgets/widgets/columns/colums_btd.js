export const formFields = [
  {
    label: "合同日期",
    name: "ContractDate",
    type: "DatePicker",
    default:true,
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "发货人代码",
    name: "Header",
    type: "AutoComplete",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "发货人名称",
    name: "ShipperInformationName",
    type: "Input",
    disabled:true,
    ellipsis:true,
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "客户代码",
    name: "Account_Name",
    type: "AutoComplete",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "客户名称",
    name: "FullName",
    type: "Input",
    disabled:true,
    ellipsis:true,
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "合同品名",
    name: "ContractItem",
    type: "Input",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "合同类型",
    name: "ContractType",
    type: "Select",
    options: ["外销合同", "内销合同"], // 合同类型选项
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },  
  {
    label: "装运港",
    name: "ShippingPort",
    type: "AutoComplete",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "目的港",
    name: "DestinationPort",
    type: "AutoComplete",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "成交方式",
    name: "TransactionMethod",
    type: "Select",
    options: ["FOB"],
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },  
  {
    label: "付款方式",
    name: "PayMethod",
    type: "Select",
    options: ["L/C", "T/T","D/P", "D/A"],
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "溢短装%",
    name: "OverShort_Shipment",
    type: "InputNumber",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
  {
    label: "数量",
    name: "nub",
    type: "InputNumber",
    rules: [
      {
        required: true,
        message: "Please input!",
      },
    ],
  },
];
