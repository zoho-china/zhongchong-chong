export const columns = [
    {
        title: '客户货号',
        dataIndex: 'AccountprtNo',
        key: 'AccountprtNo',
        visible: true,
        width: '120px', // 定义宽度为 150 像素
        search: true // 添加 search 属性
      },
      {
        title: '商品货号',
        dataIndex: 'Product_Name',
        key: 'Product_Name',
        visible: true,
        width: '120px',
        search: true // 添加 search 属性
      },
      {
        title: '内部代码',
        dataIndex: 'internalcode',
        key: 'internalcode',
        visible: true,
        width: '140px',
        search: true // 添加 search 属性
      },
      {
        title: 'HS编码',
        dataIndex: 'HSCode',
        key: 'HSCode',
        visible: false,
      },
      {
        title: '一级分类',
        dataIndex: 'Classify1',
        key: 'Classify1',
        visible: false,
      },
      {
        title: '业务渠道',
        dataIndex: 'Businesschannels',
        key: 'Businesschannels',
        visible: false,
      },
      {
        title: '中文包装描述',
        dataIndex: 'ChinesePackagedesp',
        key: 'ChinesePackagedesp',
        visible: false,
      },      
      {
        title: '发货人',
        dataIndex: 'Header',
        key: 'Header',
        visible: true,
        width: '150px',
        search: true // 添加 search 属性
      },
      {
        title: '中文品名',
        dataIndex: 'Productname',
        key: 'Productname',
        visible: true,
        width: '170px',
        ellipsis: true, // 添加 ellipsis 属性
        search: true // 添加 search 属性
      },
      {
        title: '英文品名',
        dataIndex: 'Englishname',
        key: 'Englishname',
        visible: true,
        ellipsis: true, // 添加 ellipsis 属性
        width: '170px',
        search: true // 添加 search 属性
      },
      {
        title: '中文成份',
        dataIndex: 'Chineseingredients',
        key: 'Chineseingredients',
        visible: false,
      },
      {
        title: '中文生产要求(材质)',
        dataIndex: 'Chptrequirements_material',
        key: 'Chptrequirements_material',
        ellipsis: true, // 添加 ellipsis 属性
        visible: true,
        width: '220px',
      },
      {
        title: '中文规格',
        dataIndex: 'Chinesespecification',
        key: 'Chinesespecification',
        visible: true,
        width: '220px',
      },
      {
        title: '二级分类',
        dataIndex: 'Classify2',
        key: 'Classify2',
        visible: false,
      },
      {
        title: '佣金比例',
        dataIndex: 'Commission_Rate',
        key: 'Commission_Rate',
        visible: false,
      },
      {
        title: '使用单位',
        dataIndex: 'Usage_Unit',
        key: 'Usage_Unit',
        visible: false,
      },
      {
        title: '修改者',
        dataIndex: 'Modified_By',
        key: 'Modified_By',
        visible: false,
      },
      {
        title: '创建者',
        dataIndex: 'Created_By',
        key: 'Created_By',
        visible: false,
      },
      {
        title: '包装内盒入数',
        dataIndex: 'Nuofboxesinpg',
        key: 'Nuofboxesinpg',
        visible: false,
      },
      {
        title: '包装内盒数',
        dataIndex: 'Nuofboxeinnerbox',
        key: 'Nuofboxeinnerbox',
        visible: false,
      },
      {
        title: '包装单位',
        dataIndex: 'Packaginunit',
        key: 'Packaginunit',
        visible: false,
      },
      {
        title: '单位',
        dataIndex: 'Unit',
        key: 'Unit',
        visible: false,
      },
      {
        title: '单净重(G)',
        dataIndex: 'Netweightofinnebox',
        key: 'Netweightofinnebox',
        visible: false,
      },
      {
        title: '发货人中文名称',
        dataIndex: 'field',
        key: 'field',
        visible: false,
      },
      {
        title: '品牌报关',
        dataIndex: 'Brand',
        key: 'Brand',
        visible: false,
      },
      {
        title: '唛头',
        dataIndex: 'ShippingMark',
        key: 'ShippingMark',
        visible: false,
      },
      {
        title: '商品库分类',
        dataIndex: 'Product_Category',
        key: 'Product_Category',
        visible: false,
      },
      {
        title: '商品库图片',
        dataIndex: 'Record_Image',
        key: 'Record_Image',
        visible: false,
      },
      {
        title: '商品库所有者',
        dataIndex: 'Owner',
        key: 'Owner',
        visible: false,
      },
      {
        title: '商品库编码',
        dataIndex: 'Product_Code',
        key: 'Product_Code',
        visible: false,
      },
      {
        title: '处理者',
        dataIndex: 'Handler',
        key: 'Handler',
        visible: false,
      },
      {
        title: '外箱体积(CBM)1',
        dataIndex: 'Volume1',
        key: 'Volume1',
        visible: false,
      },
      {
        title: '外箱体积（CBM）',
        dataIndex: 'Volume',
        key: 'Volume',
        visible: false,
      },
      {
        title: '外箱净重(KG)',
        dataIndex: 'Netweight',
        key: 'Netweight',
        visible: false,
      },
      {
        title: '外箱宽(CM)',
        dataIndex: 'Cartonwidth',
        key: 'Cartonwidth',
        visible: false,
      },
      {
        title: '外箱毛重(KG)',
        dataIndex: 'Grossweight',
        key: 'Grossweight',
        visible: false,
      },
      {
        title: '外箱长(CM)',
        dataIndex: 'Lengthofouterbox',
        key: 'Lengthofouterbox',
        visible: false,
      },
      {
        title: '外箱高(CM)',
        dataIndex: 'Heightofouterbox',
        key: 'Heightofouterbox',
        visible: false,
      },
      {
        title: '外销价格',
        dataIndex: 'Exportprice',
        key: 'Exportprice',
        visible: false,
        width: '80px',
      },
      {
        title: '存档',
        dataIndex: 'Archive',
        key: 'Archive',
        visible: false,
      },
      {
        title: '审批状态',
        dataIndex: 'field1',
        key: 'field1',
        visible: false,
      },
      {
        title: '客户',
        dataIndex: 'Account',
        key: 'Account',
        visible: false,
      },
      {
        title: '币种',
        dataIndex: 'Currencys',
        key: 'Currencys',
        visible: false,
      },
      {
        title: '描述',
        dataIndex: 'Description',
        key: 'Description',
        visible: false,
      },
      {
        title: '标准商品',
        dataIndex: 'GeneralProducts',
        key: 'GeneralProducts',
        visible: false,
      },
      {
        title: '标签',
        dataIndex: 'Tag',
        key: 'Tag',
        visible: false,
      },
      {
        title: '每箱装量',
        dataIndex: 'Packagingquantityperpg',
        key: 'Packagingquantityperpg',
        visible: false,
      },
      {
        title: '海外工厂采购价',
        dataIndex: 'Overseasfactorypurchaseprice',
        key: 'Overseasfactorypurchaseprice',
        visible: false,
      },
      {
        title: '申报要素',
        dataIndex: 'Declarationelements',
        key: 'Declarationelements',
        visible: false,
      },
      {
        title: '税',
        dataIndex: 'Tax',
        key: 'Tax',
        visible: false,
      },
      {
        title: '类别',
        dataIndex: 'Category',
        key: 'Category',
        visible: false,
      },
      {
        title: '英文规格',
        dataIndex: 'Specification',
        key: 'Specification',
        visible: false,
      },
      {
        title: '配比',
        dataIndex: 'Ratio',
        key: 'Ratio',
        visible: false,
      },
  ];