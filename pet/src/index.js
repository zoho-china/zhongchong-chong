import React from 'react';
import ReactDOM from 'react-dom/client';
import './Quote/index.css';
import App1 from './Quote/quote';
import App2 from './contract/App';
import App3 from './invoice/App';
import App4 from './stocknotice/App';
import App5 from './document/App';
import App6 from './contractdata/App';
import LinkingModule9 from './stocknotice/LinkingModule9';
import App7 from './batchnumber/widgets/btchccptnnmbrtdesign';
import reportWebVitals from './Quote/reportWebVitals';

const systemToRun = 'Quote'; // 可以根据需要更改要运行的系统
const root = ReactDOM.createRoot(document.getElementById('root'));
if (systemToRun === "Quote") {
    root.render(
        <App1/>
    );
} else if (systemToRun === "contract") {
    root.render(
        <App2/>
    );
} else if (systemToRun === "invoice") {
    root.render(
        <App3/>
    );
} else if (systemToRun === "stocknotice") {
    root.render(
        <App4/>
    );
} else if (systemToRun === "document") {
    root.render(
        <App5/>
    );
}else if (systemToRun === "contractdata") {
    root.render(
        <App6/>
    );
}else if (systemToRun === "LinkingModule9") {
    root.render(
        <LinkingModule9/>
    );
}else if (systemToRun === "batchnumber") {
    root.render(
        <App7/>
    );
}
reportWebVitals();
