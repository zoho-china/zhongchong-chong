import React, {Component} from 'react';
import QuoteForm from './Form.js';
import {Tabs} from 'antd';
import InvoiceDetail from './InvoiceDetail';
import {message} from 'antd';

class App extends Component {
    constructor(props) {
        super(props);
        this.formRef = React.createRef();

        this.state = {
            activeKey: '1',
            FcontractNo: [], // 存储FcontractNo的值
            formData: {},
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.FcontractNo !== this.state.FcontractNo) {
            // FcontractNo发生变化时触发重新渲染
            this.forceUpdate();
        }
    }

    onChange = (activeKey) => {
        if (activeKey === '2') {
            const formInstance = this.formRef.current;
            const formData = formInstance.formRef.current;
            let FcontractNo = formData.getFieldValue('FcontractNo');
            console.log(FcontractNo)
            if (FcontractNo === undefined || FcontractNo.length === 0) {
                message.error('请先选择外销合同号');
                return
            }
            this.setState({activeKey, FcontractNo, formData}); // 更新activeKey和FcontractNo的值
        }
        this.setState({activeKey});
    };


    render() {
        const {activeKey, FcontractNo, formData} = this.state;
        let key = '';
        if (FcontractNo.length > 0) {
            key = FcontractNo[0].key;
        }
        return (
            <div>
                <Tabs defaultActiveKey="1" centered activeKey={activeKey} onChange={this.onChange}>
                    <Tabs.TabPane tab="主要内容" key="1">
                        <QuoteForm ref={this.formRef}/>
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="生产通知明细" key="2">
                        <InvoiceDetail key={key} FcontractNo={FcontractNo} formData={formData}/>
                    </Tabs.TabPane>
                </Tabs>
            </div>
        );
    }
}

export default App;