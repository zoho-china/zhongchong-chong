import React, {Component} from 'react';
import QuoteForm from './Form.js';
import {Button, Tabs, Spin} from 'antd';
import {message} from 'antd';
import {intIfNull, isBlank} from "../util/util";
import ContractTable from "../utilComponent/Table";
import {API_URL} from "../constants";

const ZOHO = window.ZOHO;
const dateFormat = 'YYYY-MM-DD';

class App extends Component {
    constructor(props) {
        super(props);
        this.formRef = React.createRef();
        this.contractTableRef = React.createRef();

        this.state = {
            activeKey: '1',
            Account: {}, // 存储FcontractNo的值
            formData: {},
            user: {},
            loading: false,
            contractDetail: [],

        };
    }

    async componentDidMount() {
        let user = {};
        let contractDetail = [];

        await ZOHO.CRM.CONFIG.getCurrentUser().then(function (data) {
            user = data.users[0]
        });
        if (!isBlank(window.id)) {
            let req_data = {
                parameters: {},
                method: "GET",
                url: API_URL + `/crm/v3/Foreignsacontractdetail/search?criteria=(Foreignsacontract.id:equals:${window.id})`,
                param_type: 1,
            };
            await ZOHO.CRM.CONNECTION.invoke("crm", req_data).then((data) => {
                contractDetail = data.details.statusMessage.data;
            });
        }
        this.setState({user: user, contractDetail: contractDetail});

    }

    onChange = (activeKey) => {
        if (activeKey === '2') {
            const formInstance = this.formRef.current;
            const formData = formInstance.formRef.current;
            let Account = formData.getFieldValue('Account');
            console.log(Account)
            if (isBlank(Account)) {
                message.error('请先选择客户');
                return
            }
            let Header = formData.getFieldValue('Header');
            console.log(Header)
            if (isBlank(Header?.value)) {
                message.error('请先选择发货人代码');
                return
            }
            this.setState({activeKey, Account, formData}); // 更新activeKey和FcontractNo的值
        }
        this.setState({activeKey});
    };
    getRate = () => {
        return 1;
    }
    updateTotal = (sumData) => {
        const formInstance = this.formRef.current;
        const current = formInstance.formRef.current;
        console.log(sumData)
        Object.entries(sumData)?.map(([key, value]) => {
            try {
                if (!Number.isFinite(value) || isNaN(value)) {
                    value = 0;
                }
                current.setFieldValue(key, Number(value).toFixed(4))
            } catch (error) {
                current.setFieldValue(key, 0.0)
            }
        })
    }


    handleSave = () => {
        this.setState({loading: true});
        try {
            const {user, contractDetail} = this.state
            const formInstance = this.formRef.current;
            const current = formInstance.formRef.current;
            const contractTableInstance = this.contractTableRef.current;
            const contractTableData = contractTableInstance.state.data;
            console.log(contractTableData)
            current.validateFields().then((values) => {
                values.DeliveryDate = values.DeliveryDate?.format('YYYY-MM-DD');
                values.ContractDate = values.ContractDate?.format('YYYY-MM-DD');
                values.EstimatedShippingDate = values.EstimatedShippingDate?.format('YYYY-MM-DD');
                values.Account = {
                    id: values.Account?.value,
                    name: values.Account?.label
                }
                if (isBlank(values.Header?.value)) {
                    delete values.Header;
                } else {
                    values.Header = {
                        id: values.Header?.value,
                        name: values.Header?.label
                    }
                }
                if (isBlank(values.TradingCountry?.value)) {
                    delete values.TradingCountry;
                } else {
                    values.TradingCountry = {
                        id: values.TradingCountry?.value,
                        name: values.TradingCountry?.label
                    }
                }

                if (isBlank(values.DestinationPort?.value)) {
                    delete values.DestinationPort;
                } else {
                    values.DestinationPort = {
                        id: values.DestinationPort?.value,
                        name: values.DestinationPort?.label
                    }
                }
                if (isBlank(values.Currencys?.value)) {
                    delete values.Currencys;
                } else {
                    values.Currencys = {
                        id: values.Currencys?.value,
                        name: values.Currencys?.label
                    }
                }
                if (isBlank(values.ShippingPort?.value)) {
                    delete values.ShippingPort;
                } else {
                    values.ShippingPort = {
                        id: values.ShippingPort?.value,
                        name: values.ShippingPort?.label
                    }
                }
                let config = {
                    Entity: "Foreignsacontract",
                    APIData: values,
                    Trigger: ["workflow"]
                }
                let errorMessage = "";
                contractTableData.map((item, index) => {
                    if (isBlank(item.Product)) {
                        errorMessage += `${index + 1} 行未填写商品`
                    }
                    if (isBlank(item.Quantity) && intIfNull(item.Quantity) <= 0) {
                        errorMessage += `${index + 1} 行请填写数量`
                    }
                })
                if (!isBlank(errorMessage)) {
                    message.error(errorMessage)
                    return;
                }
                if (window.id !== undefined) {
                    values.id = window.id;
                    console.log(config);
                    ZOHO.CRM.API.updateRecord(config)
                        .then(function (data) {
                            console.log(data)
                            if (data.data[0].status === "success") {
                                message.success("保存成功");
                                let inserts = [];
                                let updates = [];
                                let deletes = contractDetail.filter(item => !contractTableData.some(data => data.id === item.id))
                                    .map(item => item.id);
                                contractTableData.map(item => {
                                    item.Foreignsacontract = {
                                        id: window.id,
                                        name: values.Name
                                    }
                                    item.AccountOrderNo = values.AccountOrderNo;
                                    item.ABQuantity = item.Quantity;
                                    item.Owner = {id: user.id}
                                    if (isBlank(item.id)) {
                                        delete item.id;
                                        inserts.push(item)
                                    } else {
                                        updates.push(item)
                                    }
                                })
                                if (inserts.length > 0) {
                                    let req_data = {
                                        "parameters": {
                                            data: inserts
                                        },
                                        "headers": {},
                                        "method": "POST",
                                        "url": API_URL + `/crm/v3/Foreignsacontractdetail`,
                                        "param_type": 2
                                    };
                                    ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                                        console.log(data);
                                    });
                                }
                                if (updates.length > 0) {
                                    let req_data = {
                                        "parameters": {
                                            data: updates
                                        },
                                        "headers": {},
                                        "method": "PUT",
                                        "url": API_URL + `/crm/v3/Foreignsacontractdetail`,
                                        "param_type": 2
                                    };
                                    ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                                        console.log(data);
                                    });
                                }
                                if (deletes.length > 0) {
                                    let req_data = {
                                        "parameters": {},
                                        "headers": {},
                                        "method": "DELETE",
                                        "url": API_URL + `/crm/v3/Foreignsacontractdetail?ids=${deletes}&wf_trigger=true`,
                                        "param_type": 2
                                    };
                                    ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                                        console.log(data);
                                    });
                                }
                                ZOHO.CRM.UI.Record.open({Entity: "Foreignsacontract", RecordID: window.id})
                                    .then(function (data) {
                                        console.log(data)
                                    })

                            } else {
                                message.error("保存失败");
                            }
                        })
                } else {
                    ZOHO.CRM.API.insertRecord(config).then(function (data) {
                        let id = data.data[0].details.id;
                        contractTableData.map(item => {
                            item.Foreignsacontract = {
                                id: id,
                                name: values.Name
                            }
                            item.AccountOrderNo = values.AccountOrderNo;
                            item.ABQuantity = item.Quantity;
                            item.Owner = {id: user.id}

                            delete item.id;
                        })
                        let req_data = {
                            "parameters": {
                                data: contractTableData
                            },
                            "headers": {},
                            "method": "POST",
                            "url": API_URL + `/crm/v3/Foreignsacontractdetail`,
                            "param_type": 2
                        };
                        ZOHO.CRM.CONNECTION.invoke("crm", req_data).then(function (data) {
                            console.log(data);
                        });
                        message.success("保存成功");
                        ZOHO.CRM.UI.Record.open({Entity: "Foreignsacontract", RecordID: id})
                            .then(function (data) {
                                console.log(data)
                            })
                    });
                }
                console.log(values)
                // current.resetFields();
            }).catch(e => {
                console.log(e);
            });
        } catch (e) {
            message.error(e)
        } finally {
            setTimeout(() => {
                this.setState({loading: false});
            }, 2000);
        }

    };

    render() {
        const {activeKey, loading} = this.state;

        return (
            <div>
                <Spin spinning={loading}>

                    <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 16}}>
                        {activeKey === '2' && (
                            <div>
                                <Button disabled={loading} onClick={this.handleSave} type="primary"
                                        style={{marginRight: 10}}>
                                    保存
                                </Button>
                                <Button style={{marginLeft: 8, marginRight: 8}} onClick={this.handleCancel}>
                                    取消
                                </Button>
                            </div>
                        )}
                    </div>
                    <Tabs defaultActiveKey="1" centered activeKey={activeKey} onChange={this.onChange}>
                        <Tabs.TabPane tab="主要内容" key="1">
                            <QuoteForm ref={this.formRef}/>
                        </Tabs.TabPane>
                        <Tabs.TabPane tab="合同明细" key="2">
                            <ContractTable ref={this.contractTableRef} moduleName="Foreignsacontractdetail"
                                           parentModuleName="Foreignsacontract"
                                           getRate={this.getRate}
                                           updateTotal={this.updateTotal}
                                           template="contractTemplate"
                                           jsonName="contracttableconf.json" paste={false}/>
                        </Tabs.TabPane>
                    </Tabs>
                </Spin>
            </div>
        );
    }
}

export default App;